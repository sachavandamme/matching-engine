package mongo

import (
    "github.com/Skyhark-Projects/dex/log"
    "github.com/mongodb/mongo-go-driver/x/network/wiremessage"
    "github.com/mongodb/mongo-go-driver/x/network/result"
    "github.com/mongodb/mongo-go-driver/x/bsonx/bsoncore"
    "errors"
    "time"
    "os"
)

type handlerCallbacks map[string]func(wiremessage.Query)([]interface{}, error)
var querhHandlers handlerCallbacks = handlerCallbacks{}
var startTime time.Time

func AddQueryHandler(key string, cb func(wiremessage.Query)([]interface{}, error)) {
    querhHandlers[key] = cb
}

func HandleQuery(query wiremessage.Query) (bool, []interface{}, error) {
    if err := query.Query.Validate(); err != nil {
        return false, []interface{}{}, err
    }

    val := bsoncore.Document(query.Query)
    for key, cb := range querhHandlers {
        if _, err := val.LookupErr(key); err == nil {
            i, e := cb(query)
            return true, i, e
        }
    }

    log.Error("Unknown query type received", "query", query.Query)
    return false, []interface{}{}, errors.New("Unknwon query type")

}

func isMaster(query wiremessage.Query) ([]interface{}, error) {
    master := result.IsMaster{
        Arbiters:                     []string{},
        ArbiterOnly:                  false,
        //ClusterTime:                  bsonx.Doc{},
        Compression:                  []string{},
        //ElectionID:                   objectid.ObjectID,
        Hidden:                       false,
        Hosts:                        []string{ "127.0.0.1" },
        IsMaster:                     true,
        IsReplicaSet:                 true,
        //LastWriteTimestamp:           time.Time,
        LogicalSessionTimeoutMinutes: 60,
        MaxBSONObjectSize:            0xffff,
        MaxMessageSizeBytes:          48000000,
        MaxWriteBatchSize:            1000,
        Me:                           "DEX",
        MaxWireVersion:               5,
        MinWireVersion:               0,
        Msg:                          "DEX Matching engine",
        OK:                           1,
        Passives:                     []string{},
        ReadOnly:                     false,
        SaslSupportedMechs:           []string{},
        Secondary:                    false,
        SetName:                      "DEX",
        SetVersion:                   3,
        Tags:                         map[string]string{},
    }

    return []interface{}{
        master,
    }, nil
}

type ServerStatus struct {
    Host string `bson:"host"`
    Version string `bson:"version"`
    Process string `bson:"process"`
    Pid int `bson:"pid"`
    Uptime int `bson:"uptime"`
    UptimeMillis int `bson:"uptimeMillis"`
    UptimeEstimate int `bson:"uptimeEstimate"`
    LocalTime time.Time `bson:"localTime"`
    Ok float32 `bson:"ok"`
}

func serverStatus(query wiremessage.Query) ([]interface{}, error) {
    uptime := int(time.Since(startTime) / time.Millisecond)
    master := ServerStatus{
        Host: "dex",
        Version: "3.0.0",
        Process: "mongod",
        Pid: os.Getpid(),
        Uptime: uptime / 1000,
        UptimeMillis: uptime,
        UptimeEstimate: uptime / 1000,
        LocalTime: time.Now(),
        Ok: 1,
    }

    return []interface{}{
        master,
    }, nil
}

type DatabaseInfo struct {
    Name string `bson:"name"`
    SizeOnDisk float64   `bson:"sizeOnDisk"`
    Empty bool  `bson:"empty"`
} 

type ListDatabases struct {
    Databases []DatabaseInfo `bson:"databases"`
    TotalSize float64 `bson:"totalSize"`
    Ok float64 `bson:"ok"`
}

func listDatabases(query wiremessage.Query) ([]interface{}, error) {
    master := ListDatabases{
        Databases: []DatabaseInfo{
            DatabaseInfo{
                Name: "dex",
                SizeOnDisk: 83886080,
                Empty: false,
            },
        },
        TotalSize: 83886080,
        Ok: 1.0,
    }

    return []interface{}{
        master,
    }, nil
}

func listCollections(query wiremessage.Query) ([]interface{}, error) {
    list := []interface{}{}
    for _, val := range collections {
        list = append(list, val)
    }

    return list, nil
}

func queryCount(query wiremessage.Query) ([]interface{}, error) {
    name := query.Query.Lookup("count").StringValue()
    collection, oke := collections[name]
    if !oke {
        return  []interface{}{}, errors.New("collection not found")
    }

    return []interface{}{
        collection.Count(),
    }, nil
}

func queryCollStats(query wiremessage.Query) ([]interface{}, error) {
    name := query.Query.Lookup("collStats").StringValue()
    collection, oke := collections[name]
    if !oke {
        return  []interface{}{}, errors.New("collection not found")
    }

    return []interface{}{
        collection.Stats(),
    }, nil
}

func getInt(query wiremessage.Query, name string, def int32) int32 {
    val := query.Query.Lookup(name)
    r, oke := val.Int32OK()
    if oke {
        return r
    }

    return def
}

func queryFind(query wiremessage.Query) ([]interface{}, error) {
    name := query.Query.Lookup("find").StringValue()
    collection, oke := collections[name]
    if !oke {
        return  []interface{}{}, errors.New("collection not found")
    }

    start := getInt(query, "skip", 0)
    limit := getInt(query, "limit", 100000000)
    return collection.Find(start, limit), nil
}

func init() {
    startTime = time.Now()
    AddQueryHandler("ismaster", isMaster)
    AddQueryHandler("serverStatus", serverStatus)
    AddQueryHandler("listDatabases", listDatabases)
    AddQueryHandler("listCollections", listCollections)
    AddQueryHandler("count", queryCount)
    AddQueryHandler("collStats", queryCollStats)
    AddQueryHandler("find", queryFind)
}