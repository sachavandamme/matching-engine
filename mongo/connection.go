package mongo

import (
    "github.com/Skyhark-Projects/dex/log"
    "github.com/mongodb/mongo-go-driver/x/network/wiremessage"
    "github.com/mongodb/mongo-go-driver/bson"
    "errors"
    "net"
)

type Connection struct {
    conn net.Conn
}

func NewConnection(conn net.Conn) *Connection {
    c := &Connection{
        conn,
    }

    go c.listen()
    return c
}

func (c *Connection) proxy(src net.Conn, dest net.Conn, proxy bool) error {
    isFirst := false
    for {
        buf := make([]byte, 0xffff)
        n, err := src.Read(buf)

        if err != nil {
            return err
        }

        header, _ := wiremessage.ReadHeader(buf, 0)
        if proxy {
            log.Debug("received header", "proxy", proxy, "header", header, "n", n)
        } else {
            log.Info("received header", "proxy", proxy, "header", header, "n", n)
        }

        //Read missing bytes:
        for int32(n) < header.MessageLength {
            buf2 := make([]byte, 0xffff)
            n2, err := src.Read(buf2)
            if err != nil {
                return err
            }

            buf = append(buf[:n], buf2[:n2]...)
            n = n+n2
        }

        if header.OpCode == wiremessage.OpQuery {
            var query wiremessage.Query
            if err := query.UnmarshalWireMessage(buf[:n]); err != nil {
                return err
            }

            intercept, intf, err := HandleQuery(query)
            if intercept {
                _, err := c.transformReply(&header, intf, err)
                if err != nil {
                    log.Error("error transform reply", "error", err)
                } /*else {
                    dest.Write(bs)
                }*/
            }

            log.Info("query", "query", query)
        } else if header.OpCode == wiremessage.OpReply {
            if isFirst {
                isFirst = false
                continue
            }
            var query wiremessage.Reply
            if err := query.UnmarshalWireMessage(buf[:n]); err != nil {
                return err
            }

            log.Debug("reply", "reply", query)
        }

        dest.Write(buf[:n])
    }
}

func (c *Connection) listen2() {
    log.Info("New client connected")
    proxy, err := net.Dial("tcp", "localhost:27017")
    if err != nil {
        log.Error("error connecting to proxy mongo", "err", err)
        return
    }

    closed := false
    stop := func() {
        if closed {
            return
        }

        closed = true
        log.Info("closing connections")
        proxy.Close()
        c.Close()
    }

    go func() {
        defer stop()
        err := c.proxy(c.conn, proxy, false)
        if err.Error() != "EOF" && !closed {
            log.Error("error reading client data", "err", err)
        }
    }()

    go func() {
        defer stop()
        err := c.proxy(proxy, c.conn, true)
        if err.Error() != "EOF" && !closed {
            log.Error("error reading proxy data", "err", err)
        }
    }()
}

func (c *Connection) transformReply(header *wiremessage.Header, res []interface{}, err error) ([]byte, error) {
    reply := wiremessage.Reply{
        ResponseFlags:  wiremessage.AwaitCapable,
        CursorID:       0,
        StartingFrom:   0,
        NumberReturned: 0,
    }

    if err != nil {
        reply.ResponseFlags = wiremessage.QueryFailure
        reply.NumberReturned = 1

        b, err3 := bson.Marshal(bson.D{{"$err", err.Error()}})
        if err3 != nil {
            log.Error("BSON Marshal error on err encoding", "error", err3, "encoding error", err)
        }

        reply.Documents = []bson.Raw{
            b,
        }
    } else {
        reply.Documents = []bson.Raw{}
        reply.NumberReturned = int32(len(res))
        for _, i := range res {
            b, err := bson.Marshal(i)
            if err != nil {
                log.Error("BSON Marshal error on encoding result", "error", err, "interface", i)
            }

            reply.Documents = append(reply.Documents, b)
        }
    }

    reply.MsgHeader = wiremessage.Header{
        RequestID:  header.RequestID,
        ResponseTo: header.RequestID,
        OpCode:     wiremessage.OpReply,
        MessageLength: int32(reply.Len()),
    };

    //log.Warn("Intercepted query", "bs",reply)
    return reply.MarshalWireMessage()
}

func (c *Connection) listen() {
    log.Info("New mongodb client connected")
    for {
        buf, header, err := c.Read()
        if err != nil {
            if err.Error() != "EOF" {
                log.Error("Mongo connection read error", "error", err)
            }

            log.Info("Mongo client connection closed")
            c.Close()
            return
        }

        res, err := c.handle(buf, header)
        buf, err = c.transformReply(header, res, err)

        if err != nil {
            log.Error("Marshal mongo response error", "err", err)
            c.Close()
        } else {
            c.conn.Write(buf)
        }
    }
}

func (c *Connection) handle(buf []byte, header *wiremessage.Header) ([]interface{}, error) {
    if header.OpCode == wiremessage.OpQuery {
        var query wiremessage.Query
        err := query.UnmarshalWireMessage(buf)
        if err != nil {
            return []interface{}{}, err
        }

        _, res, err := HandleQuery(query)
        return res, err
    }

    log.Error("Mongo unknown command OpCode received", "header", header)
    return []interface{}{}, errors.New("Unknown OpCode received")
}

func (c *Connection) Read() ([]byte, *wiremessage.Header, error) {
    buf := make([]byte, 0xffff)
    n, err := c.conn.Read(buf)
    if err != nil {
        return buf, nil, err
    }

    header, err := wiremessage.ReadHeader(buf, 0)
    if err != nil {
        return buf, nil, err
    }
    
    //Read missing bytes:
    for int32(n) < header.MessageLength {
        buf2 := make([]byte, 0xffff)
        n2, err := c.conn.Read(buf2)
        if err != nil {
            return buf, nil, err
        }

        buf = append(buf[:n], buf2[:n2]...)
        n = n+n2
    }

    return buf, &header, err
}

func (c *Connection) Close() {
    c.conn.Close()
}