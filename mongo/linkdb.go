package mongo

import (
    "github.com/Skyhark-Projects/dex/ethdb"
)

func LinkLDB(name string, db *ethdb.LDBDatabase, createInterface func([]byte, []byte) interface{}) {
    NewCollection(name, &LDB{
        db: db,
        createInterface: createInterface,
    })
}

//-------------------------------------------------

type LDB struct {
    db *ethdb.LDBDatabase
    createInterface func([]byte, []byte) interface{}
}

func (b *LDB) Count() int {
    it := b.db.NewIterator()
    count := 0
    defer it.Release()
	for it.Next() {
        count++
    }

    return count
}

func (b *LDB) Size() int {
    return 0
}

func (b *LDB) Find(start int32, limit int32) []interface{} {
    res := []interface{}{}
    it := b.db.NewIterator()
    defer it.Release()
    i := int32(0)

	for it.Next() {
        if i < start {
            i++
            continue
        } else if i >= start + limit {
            break
        }

        tmp := b.createInterface(it.Key(), it.Value())
        res = append(res, tmp)
        i++
    }

    return res
}