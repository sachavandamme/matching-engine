package mongo

import (
    "math"
    "sync"
)

type CollectionInterface interface {
    Count() int
    Size() int
    Find(int32, int32) []interface{}
}

//-------------------------------------------------

type CollectionInfo struct {
    ReadOnly bool `bson:"readOnly"`
}

type IdIndex struct {
    V int `bson:"v"`
    Key map[string]int `bson:"key"`
    Name string `bson:"name"`
    Ns string `bson:"ns"`
}

type Collection struct {
    Name string `bson:"name"`
    Type string `bson:"type"`
    Options map[string]interface{} `bson:"options"`
    Info CollectionInfo `bson:"info"`
    IdIndex IdIndex `bson:"idIndex"`
    src CollectionInterface `bson:omit`
}

type QueryCount struct {
    N int `bson:"n"`
    Ok float64 `bson:"ok"`
}

type CollectionStats struct {
    Ns string `bson:"ns"`
    Size int `bson:"size"`
    Count int `bson:"count"`
    AvgObjectSize int `bson:"avgObjectSize"`
    NumExtends int `bson:"numExtends"`
    StorageSize int `bson:"storageSize"`
    LastExtentSize float64 `bson:"lastExtentSize"`
    PaddingFactor float64 `bson:"paddingFactor"`
    PaddingFactorNote string `bson:"paddingFactorNote"`
    UserFlags int `bson:"userFlags"`
    Capped bool `bson:"capped"`
    Nindexes int `bson:"nindexes"`
    IndexDetails map[string]interface{} `bson:"indexDetails"`
    TotalIndexSize int `bson:"totalIndexSize"`
    IndexSizes map[string]int `bson:"indexSizes"`
    Ok float64 `bson:"ok"`
}

var collections = map[string]Collection{}
var collectionsLock = sync.RWMutex{}

//---------

func NewCollection(name string, src CollectionInterface) Collection {
    coll := Collection{
        Name: name,
        Type: "collection",
        Options: map[string]interface{}{},
        Info: CollectionInfo{
            ReadOnly: true,
        },
        IdIndex: IdIndex{
            V: 2,
            Key: map[string]int{
                "_id": 1,
            },
            Name: "_id_",
            Ns: "dex." + name,
        },
        src: src,
    }

    collectionsLock.Lock()
    defer collectionsLock.Unlock()
    collections[name] = coll
    return coll
}

func (c *Collection) Count() QueryCount {
    return QueryCount{
        N: c.src.Count(),
        Ok: 1,
    }
}

func (c *Collection) Stats() CollectionStats {
    size := c.src.Size()
    count := c.Count().N
    
    return CollectionStats{
        Ns: "dex." + c.Name,
        Size: size,
        Count: count,
        AvgObjectSize: size / int(math.Max(float64(count), 1)),
        NumExtends: 4,
        StorageSize: 5000,
        LastExtentSize: 5,
        PaddingFactor: 1,
        PaddingFactorNote: "paddingFactor is unused and unmaintained in 3.0. It remains hard coded to 1.0 for compatibility only.",
        UserFlags: 1,
        Capped: false,
        Nindexes: 1,
        IndexDetails: map[string]interface{}{},
        TotalIndexSize: 8176,
        IndexSizes: map[string]int{
            "_id_": 8176,
        },
        Ok: 1,
    }
}

func (c *Collection) Find(start int32, limit int32) []interface{} {
    return c.src.Find(start, limit)
}
