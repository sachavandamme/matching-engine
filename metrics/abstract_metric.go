package metrics

import "bufio"

type AbstractMetric interface {
    Write(*bufio.Writer, string, string, int64)
}