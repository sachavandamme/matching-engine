package main

import (
    //"math/big"
    "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/common"
    "github.com/Skyhark-Projects/dex/accounts/abi"
    "github.com/Skyhark-Projects/dex/exchange"
    "github.com/Skyhark-Projects/dex/kyc"
    "fmt"
    "path"
    "path/filepath"
    "os"
)


func pushArgument(Args abi.Arguments, Name string, Type string) abi.Arguments {
    return exchange.PushArgument(Args, Name, Type);
}

type Response struct {
    Test string
    Test2 string
    Test3 string
    //Matched bool
    //Dependencies[] string
    //Routes[] string
    //RouteValues[] uint
}

type ResponseParse struct {
    Test string
    Test2 string
    Test3 string
    //Matched bool
    //Dependencies[] [32]byte
    //Routes[] [32]byte
    //RouteValues[] *big.Int
}

func (r *ResponseParse) Arguments() abi.Arguments {
    /*Args := abi.Arguments{}
    Args = pushArgument(Args, "Test", "string");
    Args = pushArgument(Args, "Test2", "string");
    //Args = pushArgument(Args, "Dependencies", "bytes32[]");
    //Args = pushArgument(Args, "Routes", "bytes32[]");
    //Args = pushArgument(Args, "RouteValues", "uint256[]");
    
    return Args;*/
    return exchange.GetAbiArguments(r);
}

func (r *ResponseParse) Unpack(result[] byte) error {
    return exchange.UnpackAbi(r, result);
}

func (r *ResponseParse) Pack() ([]byte, error) {
    Args := r.Arguments();
    return Args.Pack(r.Test, r.Test2);
}

func (r *Response) Parse(result[] byte) error {

    /*parser2 := ResponseParse{
        Test: "Halloah",
        Test2: "ABCD",
    };

    bt, err := parser2.Pack();
    
    if(err != nil) {
        return err;
    }
    
    log.Debug("test", "pack", common.ToHex(bt))*/

    //-----

    parser := ResponseParse{};
    err := parser.Unpack(result);
    
    if(err != nil) {
        return err;
    }

    //log.Debug("Unpack", "res", parser)
    
    //----------------
    
    r.Test = parser.Test;
    r.Test2 = parser.Test2;
    r.Test3 = parser.Test3;
    
    /*r.Matched = parser.Matched;
    
    for _, element := range parser.Dependencies {
        r.Dependencies = append(r.Dependencies, string(element[:]));
    }

    for _, element := range parser.Routes {
        r.Routes = append(r.Routes, string(element[:]));
    }*/
    
    return nil;
}

func packId() *kyc.IdentitySignature {
    dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

    root := kyc.GetRoot()
    root.AddAuthorityFile(path.Join(dir, "../../kyc/ssl-test/ca/ca.crt"))
    
    identity := kyc.FromFile(path.Join(dir, "../../kyc/ssl-test/identity/identity.key"))
    identity.LoadPem(path.Join(dir, "../../kyc/ssl-test/identity/identity.crt"))
    
    idSig := kyc.IdentitySignature{
        Token: identity.GetToken(),
        Signature: []byte{},
    }

    /*pack, err := idSig.PackABI();

    if(err != nil) {
        log.Error("Unpack", "error", err)
    } else {
        log.Info("Unpack", "pack", common.ToHex(pack));
    }

    fmt.Println("\n");
    //fmt.Println(common.Hex2Bytes("0000000000000000000000000000000000000000000000000000000000000020"));*/
    
    return &idSig;
}

func packOrderToken() {
    ///PackABI
    token, err := exchange.NewOrderToken(5000, "BCH", "000ADDRR00");

    pack, err := token.PackABI();
    if(err != nil) {
        log.Error("Unpack", "error", err)
    } else {
        log.Info("Unpack", "pack", common.ToHex(pack));
    }
    
    fmt.Println("\n");
}

func packOrder() {
    order := exchange.New();
    //order.Identity = packId();
    order.Timestamp = 0;
    order.StrictIdentity = false;
    order.RequestedToken.Quantity = 5000;
    order.RequestedToken.Iso = "BCH";
    order.RequestedToken.Address = "000ADDRR00";

    //order.OfferedToken = exchange.NewOrderToken(200, "TRX", "ABCD");

    //---------

    pack, err := exchange.PackSmartContractOrders(order, 500, order, 200, exchange.Match{});

    if(err != nil) {
        log.Error("Unpack", "error", err)
    } else {
        log.Info("Unpack", "pack", common.ToHex(pack));
    }

    fmt.Println("\n");
}

func test(res[] byte) {
    //packId();
    //packOrderToken();
    packOrder()
    
    response := Response{};
    //arg := response.GetAutoArguments();
    //arg := response.GetArguments();

    //err := arg.Unpack(&response, res)
    
    err := response.Parse(res);
    
    if(err != nil) {
        log.Error("Unpack", "error", err)
    } else {
        log.Debug("Unpack", "res", response)
    }
}
