package main

import (
    "os"
    "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/mongo"
)


func main() {
    log.Root().SetHandler(log.LvlFilterHandler(log.LvlTrace, log.StreamHandler(os.Stderr, log.TerminalFormat(true))))

    mongo.Start()
}