package main

import (
    "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/rpc"
    "context"
    "time"
)

type Source struct {
    Uri string
    client *rpc.Client
    Markets []string
    lastLookup uint
    closed bool
}

func NewSource(rawurl string) (*Source, error) {
    client, err := rpc.Dial(rawurl)
    if err != nil {
        return nil, err
    }

    src := &Source{
        Uri: rawurl,
        client: client,
        Markets: []string{},
        lastLookup: 0,
    }

    _, err = src.LoadMarkets()
    return src, err
}

func (src *Source) LoadMarkets() ([]string, error) {
    now := uint(time.Now().UnixNano() / int64(time.Millisecond))
    if src.lastLookup > now - 10000  {
        return src.Markets, nil
    }

    src.lastLookup = now
    res := []string{}
    if err := src.Call(&res, "dex_availableMarkets"); err != nil {
        log.Error("loading markets", "error", err)
        return src.Markets, err
    }

    src.Markets = res
    return res, nil
}

func (src *Source) Call(res interface{}, method string, args ...interface{}) error {
    return src.client.Call(res, method, args...)
}

func (src *Source) DexSubscribe(ctx context.Context, channel interface{}, args ...interface{}) (*rpc.ClientSubscription, error) {
    return src.client.DexSubscribe(ctx, channel, args...)
}

func (src *Source) Closed() bool {
    return src.closed
}

func (src *Source) Close() {
    src.client.Close()
    src.closed = true
}