package main

import (
    "github.com/Skyhark-Projects/dex/p2p/discover"
    "github.com/Skyhark-Projects/dex/rpc"
    "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/node"
    "github.com/Skyhark-Projects/dex/p2p"
    "strings"
    "context"
    "errors"
    "time"
)

type RPCService struct {
    service *exchangeService
    pool *Pool
}

func (s *RPCService) Src(market string) (*Source, error) {
    src := s.pool.Src(market)
    if src == nil {
        return nil, errors.New("Source not found for the requested market " + market)
    }

    return src, nil
}

func (s *RPCService) AvailableMarkets() []string {
    return s.pool.GetMarkets()
}

func (s *RPCService) OrderBook(market string, limit uint) (map[string]interface{}, error) {
    src, err := s.Src(market)
    res := map[string]interface{}{}
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_orderBook", market, limit)
    return res, err
}

func (s *RPCService) OrderBookWithTimeAgo(market string, limit uint) (map[string]interface{}, error) {
    src, err := s.Src(market)
    res := map[string]interface{}{}
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_orderBookWithTimeAgo", market, limit)
    return res, err
}

func (s *RPCService) GetChannelsConfig() (map[string]interface{}, error) {
    s.pool.RLock()
    defer s.pool.RUnlock()
    res := map[string]interface{}{}
    if len(s.pool.Sources) == 0 {
        return res, errors.New("No valable data source found")
    }

    src := s.pool.Sources[0]
    //TODO merge respones of all data sources

    err := src.Call(&res, "dex_getChannelsConfig")
    return res, err
}

func (s *RPCService) Summaries(ctx context.Context, markets []string) (*rpc.Subscription, error) {
    return &rpc.Subscription{}, nil
    /*s.pool.RLock()
    defer s.pool.RUnlock()
    if len(s.pool.Sources) == 0 {
        return nil, errors.New("No valable data source found")
    }

    src := s.pool.Sources[0]

    //ToDo splut markets to multiple src & aggregate all summaries
    //------------------------------------------------------------
    notifier, supported := rpc.NotifierFromContext(ctx)
    if !supported {
        return nil, rpc.ErrNotificationsUnsupported
    }

    //clientCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
    SummaryChan := make(chan []interface{})
    client, err := src.DexSubscribe(ctx, &SummaryChan, "summaries", markets)
    if err != nil {
        return nil, err
    }

    rpcSub := notifier.CreateSubscription()
    go func() {
        defer client.Unsubscribe()

        for {
            select {
                case summaries := <-SummaryChan:
                    log.Info("Client subscription error", "err", summaries)
                case err := <-client.Err():
                    log.Error("Client subscription error", "err", err)
                    return
                case err := <-rpcSub.Err():
                    log.Error("RPC subscription error", "err", err)
                    return
                case <-notifier.Closed():
                    return
            }
        }
    }()

    return rpcSub, nil*/
}

func (s *RPCService) History(ctx context.Context, market string) (*rpc.Subscription, error) {
    notifier, supported := rpc.NotifierFromContext(ctx)
    if !supported {
        log.Error("history subscribe error", "error", rpc.ErrNotificationsUnsupported)
        return &rpc.Subscription{}, rpc.ErrNotificationsUnsupported
    }

    rpcSub := notifier.CreateSubscription()
    closed := false

    go func() {
        defer log.Info("unsubscibed market", "market", market)

        lastError := ""
        for !closed {
            marketSrc := market
            marketSub := market
            spl := strings.Split(market, "-")
            if len(spl) > 1 && spl[1] == "*" { //ToDo subscribe * of all sources
                base := spl[0]
                for _, available := range s.pool.GetMarkets() {
                    spl2 := strings.Split(available, "-")
                    if spl2[0] == base && spl2[1] != "USDT" && !(spl2[1] == "BTC" && base == "ETH") {
                        marketSrc = available
                        break
                    }
                }

                marketSub = "*"
            }

            src, err := s.Src(marketSrc)
            if err != nil {
                if err.Error() != lastError {
                    log.Error("history subscribe error", "error", err)
                    lastError = err.Error()
                }

                time.Sleep(1 * time.Second)
                continue
            }

            history := make(chan map[string]interface{})
            sub, err := src.DexSubscribe(context.Background(), history, "history", marketSub)
            if err != nil {
                if err.Error() != lastError {
                    log.Error("history subscribe error", "error", err)
                    lastError = err.Error()
                }

                time.Sleep(1 * time.Second)
                continue
            }

            log.Info("did subscribe history of", "market", market, "src", marketSrc, "uri", src.Uri)
            unsubscribe := false
            for !closed && !unsubscribe {
                select {
                    case item := <-history:
                        notifier.Notify(rpcSub.ID, item)
                    case err = <-sub.Err():
                        log.Error("history subscribe error", "error", err)
                        unsubscribe = true
                    case <-rpcSub.Err():
                        closed = true
                    case <-notifier.Closed():
                        closed = true
                }
            }

            sub.Unsubscribe()
            if !closed {
                time.Sleep(1 * time.Second)
            }
        }
    }()

    return rpcSub, nil
}

//------------------------------------------------------------------------

func ServiceRegistrer(name string) func(ctx *node.ServiceContext) (node.Service, error) {
    return func(nodeCtx *node.ServiceContext) (node.Service, error) {
        service := newExchangeService(nodeCtx)
        return service, nil
    }
}

type exchangeService struct {
    log      log.Logger
    server	 *p2p.Server
    ctx      *node.ServiceContext
}

func newExchangeService(ctx *node.ServiceContext) *exchangeService {
    service := &exchangeService{
		log:    log.New("Service", "Router"),
		server: nil,
        ctx:    ctx,
	}

    return service
}

func (p *exchangeService) Protocols() []p2p.Protocol {
	return []p2p.Protocol{}
}

func (p *exchangeService) APIs() []rpc.API {
	return []rpc.API{
		{
			Namespace: "dex",
			Version:   "1.0",
			Service:   &RPCService{
                service:   p,
                pool:      NewPool(),
            },
			Public:    true,
		},
	}
}

func (p *exchangeService) Start(server *p2p.Server) error {
	p.server = server
	p.log.Info("Router service starting")
	return nil
}

func (p *exchangeService) Stop() error {
	p.log.Info("Router service stopping")
	return nil
}

func (p *exchangeService) NodeInfo() interface{} {
    return nil
}

func (p *exchangeService) PeerInfo(nodeId discover.NodeID) interface{} {
    return nil
}