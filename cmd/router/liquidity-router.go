package main

import (
    "errors"
)

func (s *RPCService) Matches(market string) ([]interface{}, error) {
    src, err := s.Src(market)
    res := []interface{}{}
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_matches", market)
    return res, err
}

/*func (s *RPCService) MatchMarkets() StringList {
    return Matches.Markets()
}*/

func (s *RPCService) WhoHasTheStock(iso string, quantity uint64) ([]string, error) {
    s.pool.RLock()
    defer s.pool.RUnlock()
    res := []string{}
    if len(s.pool.Sources) == 0 {
        return res, errors.New("No valable data source found")
    }

    src := s.pool.Sources[0]
    err := src.Call(&res, "dex_whoHasTheStock", iso, quantity)
    return res, err
}

func (s *RPCService) WhoIsLessExpensive(reqIso string, offIso string, exchanges []string) (string, error) {
    src, err := s.Src(reqIso + "-" + offIso)
    res := ""
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_whoIsLessExpensive", reqIso, offIso, exchanges)
    return res, err
}

func (s *RPCService) Buy(market string, quantity uint64, rate float64) (map[string]interface{}, error) {
    src, err := s.Src(market)
    res := map[string]interface{}{}
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_buy", market, quantity, rate)
    return res, err
}

func (s *RPCService) Sell(market string, quantity uint64, rate float64) (map[string]interface{}, error) {
    src, err := s.Src(market)
    res := map[string]interface{}{}
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_sell", market, quantity, rate)
    return res, err
}


func (s *RPCService) WhichBuyExchange(market string, quantity uint64, rate float64) (string, error) {
    src, err := s.Src(market)
    res := ""
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_whichBuyExchange", market, quantity, rate)
    return res, err
}

func (s *RPCService) WhichSellExchange(market string, quantity uint64, rate float64) (string, error) {
    src, err := s.Src(market)
    res := ""
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_whichSellExchange", market, quantity, rate)
    return res, err
}

func (s *RPCService) MarketExists(exchange string, market string) (bool, error) {
    src, err := s.Src(market)
    res := false
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_marketExists", exchange, market)
    return res, err
}

func (s *RPCService) Balances(exchangeName string) ([]interface{}, error) {
    s.pool.RLock()
    defer s.pool.RUnlock()
    res := []interface{}{}
    if len(s.pool.Sources) == 0 {
        return res, errors.New("No valable data source found")
    }

    src := s.pool.Sources[0]
    err := src.Call(&res, "dex_balances", exchangeName)
    return res, err
}

func (s *RPCService) TotalBalances() (map[string]interface{}, error) {
    s.pool.RLock()
    defer s.pool.RUnlock()
    res := map[string]interface{}{}
    if len(s.pool.Sources) == 0 {
        return res, errors.New("No valable data source found")
    }

    src := s.pool.Sources[0]
    //TODO merge respones of all data sources

    err := src.Call(&res, "dex_totalBalances")
    return res, err
}

func (s *RPCService) Exchanges() (map[string]interface{}, error) {
    s.pool.RLock()
    defer s.pool.RUnlock()
    res := map[string]interface{}{}
    if len(s.pool.Sources) == 0 {
        return res, errors.New("No valable data source found")
    }

    src := s.pool.Sources[0]
    //TODO merge respones of all data sources

    err := src.Call(&res, "dex_exchanges")
    return res, err
}

func (s *RPCService) GetLiquidityOrder(exchangeName string, market string, uuid string) (map[string]interface{}, error) {
    src, err := s.Src(market)
    res := map[string]interface{}{}
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_getLiquidityOrder", exchangeName, uuid)
    return res, err
}

func (s *RPCService) CancelLiquidityOrder(exchangeName string, market string, uuid string) (map[string]interface{}, error) {
    src, err := s.Src(market)
    res := map[string]interface{}{}
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_cancelLiquidityOrder", exchangeName, uuid)
    return res, err
}

/*func (s *RPCService) GetMatchLogs(exchangeName string, uuid string) ([]MatchRequest, error) {
    exchange := GetExchange(exchangeName)
    if exchange == nil {
        return []MatchRequest{}, errors.New("Exchange not found")
    }

    match, err := LoadMatchLog(exchange, uuid)
    if err != nil {
        return []MatchRequest{}, err
    }

    return match.Requests, nil
}

func (s *RPCService) GetAllMatches(exchangeName string) ([]string, error) {
    exchange := GetExchange(exchangeName)
    if exchange == nil {
        return []string{}, errors.New("Exchange not found")
    }

    res := []string{}
    it := exchange.MatchesDb.NewIterator()
    defer it.Release()

	for it.Next() {
        res = append(res, string(it.Key()))
    }

    return res, nil
}*/

func (s *RPCService) GetDepositAddress(iso string) (string, error) {
    s.pool.RLock()
    defer s.pool.RUnlock()
    res := ""
    if len(s.pool.Sources) == 0 {
        return res, errors.New("No valable data source found")
    }

    src := s.pool.Sources[0]
    err := src.Call(&res, "dex_getDepositAddress", iso)
    return res, err
}

func (s *RPCService) ListRemoteOrders(exchange string, market string, start int, limit int) ( map[string]interface{}, error) {
    src, err := s.Src(market)
    res := map[string]interface{}{}
    if err != nil {
        return res, err
    }

    err = src.Call(&res, "dex_listRemoteOrders", exchange, market, start, limit)
    return res, err
}