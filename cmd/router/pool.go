package main

import (
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "github.com/Skyhark-Projects/dex/log"
    "k8s.io/apimachinery/pkg/selection"
    "k8s.io/apimachinery/pkg/labels"
    "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
    "strings"
    "time"
    "sync"
)

type Pool struct {
    RoundRobin map[string]*roundRobin
    Sources []*Source
    lock sync.RWMutex
    lastLookup uint
    K8s *kubernetes.Clientset
}

func NewPool() *Pool {
    p := &Pool {
        RoundRobin: map[string]*roundRobin{},
        Sources: []*Source{},
        lock: sync.RWMutex{},
        lastLookup: 0,
        K8s: nil,
    }

    go p.listen()
    return p
}

func (p *Pool) listen() {
    for {
        p.CleanOldData()
        time.Sleep(10 * time.Second)
    }
}

func (p *Pool) CleanOldData() {
    p.LookupNodes()

    p.lock.Lock()
    defer p.lock.Unlock()
    now := uint(time.Now().UnixNano() / int64(time.Millisecond))

    for market, round := range p.RoundRobin {
        if round.LastUsed < now - (5 * 60 * 1000) {
            delete(p.RoundRobin, market)
        }
    }

    for i, src := range p.Sources {
        if src.Closed() {
            p.Sources = append(p.Sources[:i], p.Sources[i+1:]...)
        } else {
            src.LoadMarkets()
            go p.OnMarketsDataLoaded(src)
        }
    }
}

func (p *Pool) OnMarketsDataLoaded(src *Source) {
    p.lock.RLock()
    defer p.lock.RUnlock()

    markets := src.Markets
    for market, round := range p.RoundRobin {
        hasMarket := false
        for _, m := range markets {
            if m == market {
                hasMarket = true
                break
            }
        }

        if hasMarket {
            round.Attach(src)
        } else {
            round.Detach(src)
        }
    }
}

func (p *Pool) LookupNodes() {
    now := uint(time.Now().UnixNano() / int64(time.Millisecond))
    p.lock.Lock()
    if p.lastLookup > now - 10000 {
        p.lock.Unlock()
        return
    }

    p.lastLookup = now
    oldLength := len(p.Sources)
    p.lock.Unlock()

    //--------------------------------------------

    uris := []string{}

    if k8sArg == "" {
        uris = append(uris, "ws://127.0.0.1:8546")
    } else {
        if p.K8s == nil {
            config, err := rest.InClusterConfig()
            if err != nil {
                log.Error("error getting k8s in cluster config", "error", err)
                return
            }

            clientset, err := kubernetes.NewForConfig(config)
            if err != nil {
                log.Error("error creating k8s client", "error", err)
                return
            }

            p.K8s = clientset
        }

        flagSplit := strings.Split(k8sArg, "/")
        if len(flagSplit) != 2 {
            log.Error("Wrong k8s argument provided", "error", k8sArg, "length", len(flagSplit))
            return
        }

        r, _ := labels.NewRequirement("app", selection.Equals, []string{flagSplit[1]})
        pods, err := p.K8s.CoreV1().Pods(flagSplit[0]).List(metav1.ListOptions{
            LabelSelector: r.String(),
        })

		if err != nil {
			log.Error("error gettings pods", "error", err)
            return
        }

        for _, item := range pods.Items {
            uris = append(uris, "ws://" + item.Status.PodIP + ":8546")
        }
    }

    //--------------------------------------------

    detachable := []*Source{}
    p.lock.RLock()
    for _, src := range p.Sources {
        exists := false
        for _, uri := range uris {
            if uri == src.Uri {
                exists = true
                break
            }
        }

        if !exists {
            detachable = append(detachable, src)
        }
    }
    p.lock.RUnlock()

    for _, d := range detachable {
        p.DetachNode(d)
    }

    for _, uri := range uris {
        if _, err := p.AttachNodeByUrl(uri); err != nil {
            log.Error("Error on attaching new node client", "uri", uri, "err", err)
        }
    }

    if len(p.Sources) != oldLength {
        log.Info("lookup done", "length", len(p.Sources), "uris", uris)
    }
}

func (p *Pool) AttachNodeByUrl(url string) (*Source, error) {
    p.lock.RLock()
    
    for _, src := range p.Sources {
        if src.Uri == url {
            p.lock.RUnlock()
            return src, nil
        }
    }

    p.lock.RUnlock()

    src, err := NewSource(url)
    if err != nil {
        return src, err
    }

    p.AttachNode(src)
    return src, nil
}

func (p *Pool) AttachNode(src *Source) {
    p.lock.Lock()
    defer p.lock.Unlock()
    p.Sources = append(p.Sources, src)

    for market, round := range p.RoundRobin {
        hasMarket := false
        for _, n := range src.Markets {
            if n == market {
                hasMarket = true
                break
            }
        }

        if hasMarket {
            round.Attach(src)
        }
    }
}

func (p *Pool) DetachNode(src *Source) {
    p.lock.Lock()
    defer p.lock.Unlock()

    src.Close()
    for _, round := range p.RoundRobin {
        round.Detach(src)
    }

    for i, s := range p.Sources {
        if s == src {
            p.Sources = append(p.Sources[:i], p.Sources[i+1:]...)
            break
        }
    }
}

func (p *Pool) createRoundRobin(market string) *roundRobin {
    p.lock.Lock()
    defer p.lock.Unlock()

    round := newRoundRobin()
    for _, src := range p.Sources {
        hasMarket := false

        markets, _ := src.LoadMarkets()
        go p.OnMarketsDataLoaded(src)

        for _, n := range markets {
            if n == market {
                hasMarket = true
                break
            }
        }

        if hasMarket {
            round.Attach(src)
        }
    }

    p.RoundRobin[market] = round
    return round
}

func (p *Pool) Src(market string) *Source {
    p.lock.RLock()
    roundRobin, oke := p.RoundRobin[market]
    p.lock.RUnlock()

    if !oke {
        roundRobin = p.createRoundRobin(market)
    }

    for {
        src := roundRobin.Src()
        if src == nil {
            p.LookupNodes()
            src = roundRobin.Src()
            if src == nil {
                return src
            }
        }

        if src.Closed() {
            p.DetachNode(src)
            log.Warn("Detaching closed socket", "uri", src.Uri)
        } else {
            return src
        }
    }
}

func (p *Pool) RLock() {
    p.lock.RLock()
}

func (p *Pool) RUnlock() {
    p.lock.RUnlock()
}

//-------------------------

func (p *Pool) GetMarkets() []string {
    markets := []string{}
    p.lock.RLock()
    p.lock.RUnlock()

    for _, src := range p.Sources {
        if src.Closed() {
            continue
        }

        for _, m := range src.Markets {
            exists := false
            for _, r := range markets {
                if r == m {
                    exists = true
                    break
                }
            }

            if !exists {
                markets = append(markets, m)
            }
        }
    }

    return markets
}

//-------------------------

type roundRobin struct {
    val int
    LastUsed uint
    lock sync.Mutex
    sources []*Source
}

func newRoundRobin() *roundRobin {
    return &roundRobin{
        val: 0,
        LastUsed: uint(time.Now().UnixNano() / int64(time.Millisecond)),
        lock: sync.Mutex{},
        sources: []*Source{},
    }
}

func (r *roundRobin) Attach(src *Source) {
    r.lock.Lock()
    defer r.lock.Unlock()

    has := false
    for _, s := range r.sources {
        if s == src {
            has = true
            break
        }
    }

    if !has {
        r.sources = append(r.sources, src)
    }
}

func (r *roundRobin) Detach(src *Source) {
    r.lock.Lock()
    defer r.lock.Unlock()

    for i, s := range r.sources {
        if s == src {
            r.sources = append(r.sources[:i], r.sources[i+1:]...)
            break
        }
    }
}

func (r *roundRobin) Increment(limit int) int {
    r.val = (r.val + 1) % limit
    r.LastUsed = uint(time.Now().UnixNano() / int64(time.Millisecond))
    return r.val
}

func  (r *roundRobin) Src() *Source {
    r.lock.Lock()
    defer r.lock.Unlock()

    if len(r.sources) == 0 {
        return nil
    }

    return r.sources[ r.Increment(len(r.sources)) ]
}