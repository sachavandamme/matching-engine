package main

import (
    "github.com/Skyhark-Projects/dex/cmd/utils"
	"github.com/Skyhark-Projects/dex/metrics"
    "github.com/Skyhark-Projects/dex/log"
    "gopkg.in/urfave/cli.v1"
    "net"
)

func setupGraphite(ctx *cli.Context) {
    server := ctx.GlobalString(utils.GraphiteFlag.Name)
	if server == "" {
		return
	}

    addr, err := net.ResolveTCPAddr("tcp", server)

    if err != nil {
        log.Error("Graphite exporter failed", "error", err)
    } else {
        log.Info("Setup graphite metrics", "addr", addr)
        go metrics.Graphite(metrics.DefaultRegistry, 10e9, "dex", addr)
    }
}