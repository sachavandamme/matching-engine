# Decentralised exchange / matching engine

Bit4you likes the concept of a decentralized exchange and wants to work on this kind of schemas.
We already made a fork of the ethereum blockchain and added a kind of integrated orderbook. At this time we are already using it as an internal matching engine, but we would like to let it evoluate to an autoname decentralized exchange.
Today bit4you injects his liquidity into this protocol with a propriatary code, and added some RPC calls for internal use. 

Before we made this fork, we searched an exisitng dex which could resolve everything. The complexity of that kind of concepts made it difficult to find a dex we could implement in a production environement.
We could see some nice dex's, but their's always a limitation to the decentralisation. First of all when you want to exchange between different blockchains and not only between ERC20 tokens, you are going to look at the concept of atomic swaps, which can give a nice security to the user.
The problem with this concept is that you need to trust the fact the the other users will send you back your assets in a reasoanble time. If you lock your assets for lets say 1h, their can be a big difference on the rate of a the required pair. If the matching user never sends your assets, you just get your assets back buy maybe it's to late to buy other assets again. 
So we need a protocol that has some kind of integrated communication channel in order to be able to have a fast answer and unlock the assets if no answer was received from the mathine user.

How markets pair are chosen ?
Actually they are not chosen by anyone. A user just ask some assets and require another asset back.
When you made an RPC call to get the orderbook of "USDT-BTC", the orderbook will be generated from those orders with the rate showen in USDT.
In the other hand if you want to get the orderbook of "BTC-USDT", the same orderbook will be generated but inversed with the rates showed in BTC.

What about scallability ?
While concepts as atomic swaps are well known, their are still scalability issues. That's mainly due to the fact that every single exchange is done on the blockchain of the according crypto assets.
In order to solve that kind of issues you have to create a sidenet/side blockchain, which will contains every transaction that has been made and only broadcast the final state of your wallet to the main blockchain.
That's exactly what the lightning network does. So if you want a highly scallabble dex, you will not have any other choice than using this kind of technologies.
Depending on the blockchain that is exchanged (bitcoin,ethereum,ripple,...) you will need to implement that kind of solutions faster than with other ones. This kind of solutions also resolve the probleme of fees that are paid when you open an order and then just cancel it.

Blockchain / Lightning implementation.
When you're creating a dex, it's not only about being able to make some tranasctions. You also need to be able to interact with every single blockchain you're exchanging.
That's a lot of code that needs to be deployed and not every user is going to trade the same assets as you. A good dex has to be able to give some flexibility on the implemented blockchain protocols without just accepting orders from any other blockchains as being "valid".
Let's take a simple example. You ony want to buy DASH and Bitcoin, maybe you don't care about Litecoin and doesn't wants to have a full litecoin node on your servers.
But in a decentralised protocol every single order is broadcasted to the different nodes. So how will you know that the received Litecoin order is valid (the user has locked to assets) and can be broadcast to other nodes?
How will you prevent an attacker to create a coin named "HACK1", and just broadcast a lot of orders per second ? Because no ones know if it's a real order, everyone is going to just broadcast it and you will need to handle a lot of false data.
That kind of problems make a dex a lot more complex because you will need to shard the data and route the incomming orders to the right nodes. This way other nodes will not receive data from a market they don't care about.

Sharding
When sharding the data in a decentralised network, you resolve the problem of scallability and DDOS attacks. But you need a good routing protocol blockating unauthorized orders (from unknown markets) and broadcasting other orders from authorized unknown markets to the right nodes.
You also face to the problem of a 1% attack explained in the Ethereum wiki where an attacker can takes the control of a single shard.

At bit4you we works on schemas that could resolve all those problems. These schemas will need a lot of testes and security audits.
Every single technical problem that has been resolved by our team, generated a lot of other security issues. It asked us a long brainstorming to be able to create a nice plan of a dex, and it can probably be improved with a nice community.
We plan to publish different schemas on this git to continue our brainstorming together.
A lot of problems has already been resolved and could already been implemented.