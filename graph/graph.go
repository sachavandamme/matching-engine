package main

import ( 
    "sort"
    "time"
)

type Graph struct {
    Market string
    Ticks  FullTicks
    loading bool
    lastOrderTime uint
}

func NewGraph(market string) *Graph {
    graph := Graph{
        Market: market,
        Ticks:  FullTicks{},
        loading: false,
        lastOrderTime: 0,
    }

    /*this.ready  = new Event({
        retainTrigger: true,
        console
    });

    //Retain days by gmt timezone ?
    this.lastOrderTime = 0;

    this.load().catch((err) => {
        console.error(err);
    }).then(() => {
        this.loading = false;
        this.ready.trigger();
    });*/

    return &graph
}

func DelayName(i uint) string {
    if i <= Min {
        return "min"
    } else if(i <= FiveMin) {
        return "fiveMin"
    } else if(i <= Quarter) {
        return "quarter"
    } else if(i <= ThirtyMin) {
        return "thirtyMin"
    } else if(i <= Hour) {
        return "hour"
    } else if(i <= 4 * Hour) {
        return "fourHour"
    } else if(i <= Day) {
        return "day"
    }

    return "infinity"
}

func (graph *Graph) GetDelaysTicksCount(theoretical ...bool) TimesStruct {
    if(len(theoretical) > 0 && theoretical[0]) {
        return TimesStruct{
            Min:        Delays.Min / Min,
            FiveMin:    Delays.FiveMin / FiveMin,
            Quarter:    Delays.Quarter / Quarter,
            ThirtyMin:  Delays.ThirtyMin / ThirtyMin,
            Hour:       Delays.Hour / Hour,
            FourHours:  Delays.FourHours / (Hour * 4),
            Day:        Delays.Day / Day,
        }
    }

    res := TimesStruct{
        Min:        0,
        FiveMin:    0,
        Quarter:    0,
        ThirtyMin:  0,
        Hour:       0,
        FourHours:  0,
        Day:        0,
    }

    for _, tick := range graph.Ticks {
        elapsed := tick.EndTime - tick.Time
        if elapsed == Min {
            res.Min++
        } else if elapsed == FiveMin {
            res.FiveMin++
        } else if elapsed == Quarter {
            res.Quarter++
        } else if elapsed == ThirtyMin {
            res.ThirtyMin++
        } else if elapsed == Hour {
            res.Hour++
        } else if elapsed == 4 * Hour {
            res.FourHours++
        } else if elapsed == Day {
            res.Day++
        }
    }

    return res
}

func (graph *Graph) Last() *FullTick {
    if len(graph.Ticks) == 0 {
        return &FullTick{
            Tick: Tick{
                Time: 0,
                Open: 0,
                Close: 0,
                High: 0,
                Low: 0,
                Volume: 0,
            },
            EndTime: 0,
        }
    }

    return graph.Ticks[len(graph.Ticks) - 1];
}

func (graph *Graph) GetTicks(/*interval*/) Ticks {
    /*if(!isNaN(parseInt(interval)))
        interval = DelayName(interval * min) || this.delayTime(interval);

    if(!this.delays[interval])
        throw('Unkown time interval ' + interval);

    interval = this.delayTime(interval);

    const ticks = this.ticks.filter((obj) => obj.endTime - obj.time === interval);
    const lastTime = ticks.length > 0 ? ticks[ticks.length-1].endTime : 0;
    const pushers = this.ticks.filter((obj) => obj.endTime - obj.time < interval && obj.time >= lastTime);

    for(var i=0; i<pushers.length; i++) {
        const item = pushers[i];
        const time = item.time - (item.time % interval);

        if(!ticks.find((obj) => obj.time === time)) {
            const sub =  pushers.Group(item, interval)
            sub.time = time;
            sub.endTime = time + interval;
            ticks.push(sub);
        }
    }

    return ticks.map((obj) => ({
        time:   obj.time / 1000,
        open:   obj.open,
        close:  obj.close,
        low:    obj.low,
        high:   obj.high,
        volume: obj.volume
    }));*/
    return Ticks{}
}

/*func (graph *Graph) GetTicksFor(interval string) Ticks {
    return graph.GetTicks( intervalTime )
}*/
    
func IntervalAtTime(time uint) uint {
    timeAgo := timeNow - time;

    if timeAgo < Delays.Min {
        return Min
    } else if timeAgo < Delays.FiveMin {
        return FiveMin
    } else if timeAgo < Delays.Quarter {
        return Quarter
    } else if timeAgo < Delays.ThirtyMin {
        return ThirtyMin
    } else if timeAgo < Delays.Hour {
        return Hour
    } else if timeAgo < Delays.FourHours {
        return 4 * Hour
    }

    return Day;
}

func IntervalNameAtTime(time uint) string {
    inter := IntervalAtTime(time)
    return DelayName(inter)
}

func (graph *Graph) MidnightValue() uint {
    time := now();
    tick := graph.FindTick( time - (time % 86400000) );
    return tick.Open
}

func (graph *Graph) NearestValue(/*time, startingIndex*/) {
    /*if(time <= (Date.now() + 120000) / 1000)
        time = time * 1000; //Unix add ms

    var tick, abs, nearest = null;
    const length = this.ticks.length;

    for(var i=startingIndex||0; i<length; i++) {
        tick = this.ticks[i];
        abs = Math.abs(tick.time - time)

        if(nearest === null || abs <= nearest.abs)
            nearest = {  value: tick.open, abs };
        else
            break;
    }

    nearest = (nearest || { value: 0 }).value || 0;

    if(startingIndex !== undefined)
        return { index: i, nearest }

    return nearest;*/
}

func (graph *Graph) FindTick(time uint, createTick ...bool) *FullTick {
    if time <= (timeNow + 120000) / 1000 {
        time = time * 1000; //Unix add ms
    }

    if time < timeNow - Delays.Day {
        return nil
    }

    intervalTime := IntervalAtTime(time)
    var tick *FullTick = nil
    for _, t := range graph.Ticks {
        if t.Time <= time && t.EndTime > time && t.EndTime - t.Time == intervalTime {
            tick = t
            break
        }
    }

    if tick == nil && createTick[0] {
        tick = &FullTick{
            Tick: Tick{
                Time: time - (time % intervalTime),
                Open:   0,
                Close:  0,
                Low:    0,
                High:   0,
                Volume: 0,
            },
            EndTime: time - (time % intervalTime) + intervalTime,
        }

        graph.Ticks = append(graph.Ticks, tick)
        if len(graph.Ticks) > 1 && time < graph.Ticks[len(graph.Ticks) - 1].Time {
            sort.SliceStable(graph.Ticks, func(i, j int) bool {
                return graph.Ticks[i].Time < graph.Ticks[j].Time
            })
        }
    }

    return tick
}

func (graph *Graph) PushValue(value uint, volume uint, time uint) {
    if(value == 0 || graph.loading) {
        return
    }

    tick := graph.FindTick(time, true)
    if tick == nil {
        return
    } else if tick.Open == 0 {
        tick.Open = value
        tick.Close = value
        tick.Low = value
        tick.High = value
        tick.Volume = volume
    } else {
        tick.Close = value
        tick.Volume += volume
        if value < tick.Low {
            tick.Low = value
        }

        if value > tick.High {
            tick.High = value
        }
    }

    if(graph.lastOrderTime < timeNow - Min) {
        graph.ReorderTicks()
    }
}

func (graph *Graph) ReorderTicks() {
    graph.lastOrderTime = now
    ticks := graph.ticks
    sort.SliceStable(ticks, func(i, j int) bool {
        return ticks[i].Time < ticks[j].Time
    })
    
    /*const ticks = this.ticks.sort((a, b) => a.time < b.time ? -1 : 1);
    this.ticks  = [];

    for(var x in ticks) {
        const tick = ticks[x];
        tick.volume = Math.round(tick.volume);

        if(tick.time < now - this.delays.day)
            continue; //Not importing old data (data limit)

        const neededInterval = this.getIntervalAtTime(tick.time, true);

        if(tick.endTime - tick.time >= neededInterval) {
            this.ticks.push(tick);
        } else {
            const hasBeenGrouped = this.ticks.find((obj) => obj.time <= tick.time && obj.endTime >= tick.endTime);

            if(hasBeenGrouped)
                continue;

            if(tick.time % neededInterval !== 0) {
                this.ticks.push(tick);
                continue;
            }

            const group = this.makeTicksGroup(ticks, tick, neededInterval);

            if(group) {
                this.ticks.push(group);
            } else {
                this.ticks.push(tick);
            }
        }
    }

    //-------------------------------
    //Remove bigest duplicate ticks

    this.ticks = this.ticks.filter((o, index) => {
        for(var i=index+1; i<this.ticks.length; i++) {
            const r = this.ticks[i];
            if(r.time >= o.time && r.endTime <= o.endTime && !(r.time == o.time && r.endTime == o.endTime))
                return false;

            if(r.time > o.endTime)
                break;
        }

        return true
    })

    //-------------------------------

    if(this.saving)
        return;

    this.save().catch((err) => {
        console.error(err);
    });*/
}

func (graph *Graph) ImportTicks(/*ticks, interval*/) {
    /*if(!ticks || ticks.length === 0)
        return;

    if(ticks.length > 0 && ticks[0].time < Date.now() / 1000) {
        ticks = ticks.map((obj) => {
            if(obj.time < Date.now() / 1000)
                obj.time = obj.time * 1000;

            return obj;
        });
    }

    if(!interval)
        interval = ticks.map((obj, index) => (obj.time - (index === 0 ? 0 : ticks[index - 1].time)))[1];

    if(!interval)
        return;

    const start = ticks[0].time;
    const countBefore = this.ticks.length;
    this.ticks = this.ticks.filter((obj) => obj.time < start || obj.endTime - obj.time <= interval);

    //console.log(`importing interval ${interval}, filtered ${countBefore - this.ticks.length} items, starting with ${this.ticks.length} items`);
    var pushed = 0;

    for(var key in ticks) {
        const data   = ticks[key];

        if(data.time % interval > 0) {
            if(data.time % interval > interval / 2) {
                data.time    = data.time + interval - (data.time % interval)
            } else {
                data.time    = data.time - interval + (data.time % interval)
            }
        }

        const exists = this.ticks.find((obj) => obj.time <= data.time && obj.endTime >= data.time + interval && obj.endTime - obj.time === interval);

        if(!exists) {
            pushed++;
            this.ticks.push({
                time:    data.time,
                endTime: data.time + interval,
                open:    data.open,
                close:   data.close,
                low:     data.low,
                high:    data.high,
                volume:  data.volume
            });
        }
    }

    this.reorderTicks();*/
}

func (graph *Graph) FindHole(/*ticks*/) {
    /*const interval = ticks.map((obj, index) => (obj.time - (index === 0 ? 0 : ticks[index - 1].time)))[1];
    const holes = [];

    ticks.forEach((tick, index) => {

        if(index > 0) {
            for(var i=ticks[index-1].time+interval; i<tick.time; i += interval) {
                holes.push(i);
            }
        }

    })

    return holes;*/
}

func (graph *Graph) Load() {
    /*if(!this.market)
        return;

    this.loading = true;

    const collection = this.mongo.collection('crypto-ticks');
    const data = await collection.findOne({
        _id: this.market
    });

    if(data !== null)
        this.ticks = data.ticks;

    const countBefore = 0;

    this.saving = true; //prevent save trigger

    try {
        await this.siteManager.getSession('__global__').api('internal/import-graph', {
            graph:  this,
            market: this.market
        });
    } catch(e) {
        this.saving  = false;
        this.loading = false;
        throw(e);
    }

    this.loading = false;
    await this.save();

    this.saving = false;

    if(process.options.production !== undefined)
        console.log('Market graph loaded', this.market);*/
}

func (graph *Graph) Save() {
    /*try {
        if(!this.market || this.loading) {
            this.saving = false;
            return;
        }

        this.saving = true;

        const collection = this.mongo.collection('crypto-ticks');
        const res        = await collection.findOneAndReplace({
            _id: this.market
        }, {
            _id:   this.market,
            ticks: this.ticks
        });

        if(!res.value) {
            await collection.insert({
                _id:   this.market,
                ticks: this.ticks
            });
        }

        this.saving = false;
        return res;
    } catch(e) {
        this.saving = false;
        throw(e);
    }*/
}

func now() uint {
     return uint(time.Now().UnixNano() / int64(time.Millisecond))
}

var timeNow = now()

func init() {
    go func() {
        for {
            timeNow = now()
            time.Sleep(time.Second)
        }
    }()
}