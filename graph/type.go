package graph

const (
    Min       = uint(60 * 1000)
    FiveMin   = uint(5 * Min)
    Quarter   = uint(15 * Min)
    ThirtyMin = uint(30 * Min)
    Hour      = uint(60 * Min)
    Day       = uint(24 * Hour)
)

type TimesStruct struct {
    Min       uint
    FiveMin   uint
    Quarter   uint
    ThirtyMin uint
    Hour      uint
    FourHours uint
    Day       uint
}

var Delays = TimesStruct{
    Min:        5 * Day, //Retain minute ticks 5 days
    FiveMin:    15 * Day,
    Quarter:    20 * Day,
    ThirtyMin:  30 * Day,
    Hour:       90 * Day,
    FourHours:  365 * Day,
    Day:        3 * 365 * Day, //Retain daily tick for 3 years
}

/*type TickValue struct {
    Offer uint
    Request uint
}*/

type Tick struct {
    Time    uint
    Open    uint
    Close   uint
    High    uint
    Low     uint
    Volume  uint
}

type FullTick struct {
    Tick
    EndTime uint
}

type Ticks []Tick;
type FullTicks []FullTick;

//-------------

func (ticks FullTicks) Group(from *FullTick, interval uint) FullTick {
    res := FullTick{
        Tick: Tick{
            Time:   from.Time,
            Open:   from.Open,
            Close:  from.Close,
            High:   from.High,
            Low:    from.Low,
            Volume: from.Volume,
        },
        EndTime: from.Time + interval,
    }

    for _, item := range ticks {
        if item.Time >= from.Time && item.Time < from.Time + interval {
            res.Close   = item.Close
            res.Volume  = res.Volume + item.Volume

            if item.Low < res.Low {
                res.Low = item.Low
            }
            
            if item.High > res.High {
                res.High = item.High
            }
        }
    }

    return res
}

func (ticks FullTicks) GroupFromTime(from uint, interval uint) FullTick {
    for _, item := range ticks {
        if item.Time == from {
            return ticks.Group(&item, interval)
        }
    }

    return FullTick{
        Tick: Tick{
            Time:   from,
            Open:   0,
            Close:  0,
            High:   0,
            Low:    0,
            Volume: 0,
        },
        EndTime: from + interval,
    }
}