package exchange

import (
    "github.com/Skyhark-Projects/dex/p2p"
    "github.com/Skyhark-Projects/dex/p2p/discover"
    "github.com/Skyhark-Projects/dex/kyc"
    "github.com/Skyhark-Projects/dex/log"
    "errors"
		"sync"
)

var (
    PeersPool = NewPool()
)

const (
  rulesCode    = iota
	orderMsgCode
  removeMsgCode
  identityMsgCode
  stateMsgCode
)

type Peer struct {
    peer  *p2p.Peer
    rw    p2p.MsgReadWriter
    log   log.Logger
    Rules *p2pRules
    Error  chan error
		closed bool
}

func newPeer(peer *p2p.Peer, rw p2p.MsgReadWriter) (*Peer, error) {
    p := Peer{
        peer:   peer,
        rw:     rw,
        log:    log.New("peer.id", peer.ID()),
        Error:  make(chan error),
				closed: false,
		}

    err := p.setupRules()
    if(err != nil) {
        return nil, err
    }

    return &p, nil
}

func (p *Peer) Send(code uint64, data interface{}) error {
    return p2p.Send(p.rw, code, data)
}

func (p *Peer) ReadMsg() (p2p.Msg, error) {
    return p.rw.ReadMsg()
}

func (p *Peer) ID() discover.NodeID {
    return p.peer.ID()
}

func (p *Peer) LogTrace(msg string, ctx ...interface{}) {
    p.log.Trace(msg, ctx...)
}

func (p *Peer) LogDebug(msg string, ctx ...interface{}) {
    p.log.Debug(msg, ctx...)
}

func (p *Peer) LogInfo(msg string, ctx ...interface{}) {
    p.log.Info(msg, ctx...)
}

func (p *Peer) LogWarn(msg string, ctx ...interface{}) {
    p.log.Warn(msg, ctx...)
}

func (p *Peer) LogError(msg string, ctx ...interface{}) {
    p.log.Error(msg, ctx...)
}

func (p *Peer) Crit(msg string, ctx ...interface{}) {
    p.log.Crit(msg, ctx...)
}

//-------------

func (p *Peer) setupRules() error {
    //1) Send supported markets
    ownRules := GetAcceptedChannals()

    if err := p.Send(rulesCode, ownRules); err != nil {
        return err
    }

    //2) Receive supported markets
    msg, err := p.ReadMsg()
    if err != nil {
        return err
    }

    if msg.Code != rulesCode {
        return errors.New("Channel rules not initialised yet")
    }

    channels := P2PAcceptedChannels{}
    msg.Decode(&channels)
    if(len(channels.Markets) > 1000) {
        return errors.New("The received accepted markets count exceed the accepted limit")
    }

    if(len(channels.FullProtocols) > 10) {
        return errors.New("The received accepted protocols list exceed the accepted limit")
    }

    //3) Create forwarding rules class
    p.Rules = &p2pRules{
        acceptedChannels: channels,
    }

    return nil
}

func (p *Peer) Start() error {
    PeersPool.Append(p)
    defer func() {
        go PeersPool.Remove(p)
    }()

    go p.Listen()
    go p.Sync()

    //Identities..
    go kyc.BindPeerIdentities(p.rw, identityMsgCode, p.Error)

    return <-p.Error
}

func (p *Peer) Close(err error) {
    if p.closed {
        return
    }

    log.Error("PEER", "CLOSE", err)

    p.closed = true
    p.Error <- err
}

func (p *Peer) Listen() (err error) {
    defer func() {
        if err != nil {
            p.Close(err)
        }
    }()

    for !p.closed {
        msg, err := p.ReadMsg()
        if err != nil {
            return err
				}

        switch(msg.Code) {
						case orderMsgCode:
								p.peer.AddLoad("orderMsgCode", msg.Size);
                order := Order{};
                msg.Decode(&order)

                accepted, err := p.Rules.CouldReceive(&order)
                if err != nil {
                    return err
                } else if accepted == true {
                    AppendOrder(&order, p.ID())
                }

                break;
						case removeMsgCode:
								p.peer.AddLoad("removeMsgCode", msg.Size);
                order := OrderRemoveLog{};
                msg.Decode(&order)

                accepted, err := p.Rules.CouldRemove(&order)
                if err != nil {
                    return err
                } else if accepted == true {
                    order.RemoveFromPool( p.ID() )
                }

                break;
						case identityMsgCode:
								p.peer.AddLoad("identityMsgCode", msg.Size);
                id := kyc.Identity{};
                if err := msg.Decode(&id); err != nil {
                    return err
                }
                break;
            default:
                p.LogError("unknown msg received", "code", msg.Code)
				}

        if(p.Rules.ShouldBan()) {
            return errors.New("Peer banned from market rules protocol")
        }
    }

    return nil
}

func (p *Peer) Sync() (err error) {
    defer func() {
        if err != nil {
            p.Close(err)
        }
    }()

    if err := kyc.BroadcastIdentities(p.rw, identityMsgCode); err != nil {
        return err
    }

    p.LogInfo("Broadcasting all the existing orders..")
    for _, main := range lints {
        main.RLock()
        for _, lint := range main.offerLints { 
            item := lint

            for item != nil && p.closed == false {
                if err := p.SendOrder(item.Current); err != nil {
                    main.RUnlock()
                    return err
                }

                item = item.Next
            }
        }

        main.RUnlock()
    }

    return nil
}

func (p *Peer) SendOrder(order *Order) error {
    if(p.Rules.Accepts(order) && p.closed == false) {
        if err := p.Send(orderMsgCode, order); err != nil {
            p.Close(err)
            return err
        }
    }

    return nil
}

func (p *Peer) RemoveOrder(order *OrderRemoveLog) error {
    if(p.Rules.AcceptsRemove(order) && p.closed == false) {
        if err := p.Send(removeMsgCode, order); err != nil {
            p.Close(err)
            return err
        }
    }

    return nil
}

//-------------

type PeerPool struct {
    Peers []*Peer
    mutex *sync.RWMutex
}

func NewPool() *PeerPool {
    return &PeerPool{
        mutex: &sync.RWMutex{},
    }
}

func (pool *PeerPool) Append(peer *Peer) {
    peer.LogInfo("append to peers pool..")
    
    pool.mutex.Lock()
    pool.Peers = append(pool.Peers, peer)
    pool.mutex.Unlock()
}

func (pool *PeerPool) Remove(peer *Peer) {
    peer.LogInfo("remove from peers pool..")
    defer peer.LogError("remove from peers pool..", "REMOVE", "done")

    pool.mutex.Lock()
    defer pool.mutex.Unlock()

    for i, p := range pool.Peers {
        if p == peer {
            pool.Peers = append(pool.Peers[:i], pool.Peers[i+1:]...)
            break;
        }
    }
}

func (pool *PeerPool) RLock() {
    pool.mutex.RLock()
}

func (pool *PeerPool) RUnlock() {
    pool.mutex.RUnlock()
}

func peerIsIgnored(peer *Peer, ignoredPeers ...discover.NodeID) bool {
    if peer.closed {
        return true
    }

    peerId := peer.ID()
    for _, ignoreId := range ignoredPeers {
        if peerId == ignoreId {
            return true
        }
    }

    return false
}

func (pool *PeerPool) SendOrder(order *Order, ignoredPeers ...discover.NodeID) uint {
    var count uint = 0
    pool.RLock()
    defer pool.RUnlock()

    for _, peer := range pool.Peers {
        if !peerIsIgnored(peer, ignoredPeers...) {
            peer.SendOrder(order)
            count++
        }
    }

    return count
}

func (pool *PeerPool) RemoveOrder(order *OrderRemoveLog, ignoredPeers ...discover.NodeID) uint {
    var count uint = 0
    pool.RLock()
    defer pool.RUnlock()

    //log.Info("RemoveFromPool", "sendRemove", order.OrderId)
    //defer log.Info("RemoveFromPool", "send", "Remve Done")

    for _, peer := range pool.Peers {
        if !peerIsIgnored(peer, ignoredPeers...) {
            peer.RemoveOrder(order)
            count++
        }
    }

    return count
}
