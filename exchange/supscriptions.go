package exchange

import (
	"context"
	"strings"
	"time"

	"github.com/Skyhark-Projects/dex/log"
	"github.com/Skyhark-Projects/dex/rpc"
)

func transformMarketSplits(markets []string) []TickerMarket {
	tickerMarkets := []TickerMarket{}
	for _, market := range markets {
		spl := strings.Split(market, "-")
		tickerMarkets = append(tickerMarkets, TickerMarket{
			RequestIso: spl[1],
			OfferIso:   spl[0],
		})
	}

	return tickerMarkets
}

//--------------------------------
//Tickers

type Ticker struct {
	Market    string
	Bid       uint
	Ask       uint
	Open      uint
	Last      uint
	High      uint
	Low       uint
	Volume24H uint
	MarketCap uint
}

func GenerateSummaries(markets []TickerMarket) []Ticker {
	res := []Ticker{}

	for _, market := range markets {
		orderBook := GetOrderBook(market.OfferIso, market.RequestIso)
		res = append(res, Ticker{
			Market: market.Name(),
			Bid:    orderBook.GetBid(),
			Ask:    orderBook.GetAsk(),

			Open:      0, //ToDo
			Last:      0,
			High:      0,
			Low:       0,
			Volume24H: 0,
			MarketCap: 0, //Supply * last
		})
	}

	return res
}

func (s *RPCService) Summaries(ctx context.Context, markets []string) (*rpc.Subscription, error) {
	notifier, supported := rpc.NotifierFromContext(ctx)
	if !supported {
		return &rpc.Subscription{}, rpc.ErrNotificationsUnsupported
	}

	rpcSub := notifier.CreateSubscription()
	tickerMarkets := transformMarketSplits(markets)

	go func() {
		ticker := time.NewTicker(time.Second)

		for {
			defer ticker.Stop()

			select {
			case <-ticker.C:
				notifier.Notify(rpcSub.ID, GenerateSummaries(tickerMarkets))
			case <-rpcSub.Err():
				return
			case <-notifier.Closed():
				return
			}
		}
	}()

	return rpcSub, nil
}

func TransformMarket(market string) string {
	r := strings.Split(market, "-")
	l := len(r)
	if l != 2 {
		log.Error("Wrong market provided in transform function", "market", market)
	}

	if l >= 1 && r[0] == "BCHABC" {
		r[0] = "BCH"
	} else if l >= 2 && r[1] == "BCHABC" {
		r[1] = "BCH"
	}

	if l >= 1 && r[0] == "BSV" {
		r[0] = "BCHSV"
	} else if l >= 2 && r[1] == "BSV" {
		r[1] = "BCHSV"
	}

	return strings.Join(r, "-")
}

func (s *RPCService) History(ctx context.Context, markets string) (*rpc.Subscription, error) {
	notifier, supported := rpc.NotifierFromContext(ctx)
	if !supported {
		return &rpc.Subscription{}, rpc.ErrNotificationsUnsupported
	}

	rpcSub := notifier.CreateSubscription()
	history := make(chan *HistoryItem)

	go func() {
		subscr := OnHistory.Subscribe(history)
		defer subscr.Unsubscribe()
		log.Info("did subscribe history", "market", markets)
		for {
			select {
			case item := <-history:
				item.Market = TransformMarket(item.Market)
				notifier.Notify(rpcSub.ID, item)
			case <-rpcSub.Err():
				return
			case <-notifier.Closed():
				return
			}
		}
	}()

	return rpcSub, nil
}

//--------------------------------

type TickerMarket struct {
	RequestIso string
	OfferIso   string
}

func (ticker *TickerMarket) Name() string {
	return ticker.OfferIso + "-" + ticker.RequestIso
}
