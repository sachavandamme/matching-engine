package exchange

import (
    "time"
    "errors"
    "github.com/Skyhark-Projects/dex/log"
)

type ChannelMarket struct {
    ReqIso       string
    OffIso       string
    BothSides    bool
    ProtocolName string
}

type ChannelMarketList []ChannelMarket

type P2PAcceptedChannels struct {
    Markets         ChannelMarketList
    FullProtocols   []string
    Forwarding      bool
    ForwardingLimit uint
}

func GetAcceptedChannals() *P2PAcceptedChannels {
    //ToDo create shared rules from config

    return &P2PAcceptedChannels{
        Markets: ChannelMarketList{
            /*ChannelMarket{
                ReqIso: "BTC",
                OffIso: "ZEC",
                BothSides: true,
                ProtocolName: "Generic",
            },*/
        },
        FullProtocols: []string{ "Generic", "Liquidity" },
        Forwarding: true,
        ForwardingLimit: 10,
    }
}

func (c *P2PAcceptedChannels) Accepts(order *Order) (bool, error) {
    if(order.Ttl != nil && *order.Ttl != 0 && int64(*order.Ttl + order.Timestamp) < time.Now().UnixNano() / int64(time.Millisecond)) {
        return false, errors.New("TTL Expired")
    }

    //ToDo add rule to filter orders older than 1 month

    return c.AcceptsVars(order.RequestedToken.Iso, order.OfferedToken.Iso, order.Protocol.Name()), nil
}

func (c *P2PAcceptedChannels) AcceptsVars(reqIso string, offIso string, protoName string) bool {
    for _, name := range c.FullProtocols {
        if name == protoName {
            return true
        }
    }

    for _, market := range c.Markets {
        if market.ProtocolName == protoName {
            if market.ReqIso == reqIso && market.OffIso == offIso {
                return true
            }

            if market.BothSides && market.ReqIso == offIso && market.OffIso == reqIso {
                return true
            }
        }
    }

    return false
}

//-------------

type p2pRules struct {
    acceptedChannels P2PAcceptedChannels
    forwardingRules  []string //Todo
    banScore         uint
}

func (r *p2pRules) Accepts(order *Order) bool {
    acc, err := r.acceptedChannels.Accepts(order)
    
    if(err != nil) {
        log.Error("Accepts Order", "error", err, "identity", order.Identity.GetToken(), "Timestamp", order.Timestamp, "TTL", order.Ttl, "ISO", order.RequestedToken.Iso + "-" + order.OfferedToken.Iso)
    }

    return acc
}

func (r *p2pRules) AcceptsRemove(order *OrderRemoveLog) bool {
    return r.acceptedChannels.AcceptsVars(order.RequestedIso, order.OfferedIso, order.Protocol)
}

/*func (r *p2pRules) AcceptsForwarding(order *Order) bool {
    //ToDo
    return true
}*/

func (r *p2pRules) CouldReceive(order *Order) (bool, error) {
    if(!order.VerifySignature()) {
        r.banScore = 100;
        return false, errors.New("Wrong signature received");
    }

    return GetAcceptedChannals().Accepts(order)
}

func (r *p2pRules) CouldRemove(order *OrderRemoveLog) (bool, error) {
    if(!order.VerifySignature()) {
        r.banScore = 100;
        return false, errors.New("Wrong singature received");
    }

    return GetAcceptedChannals().AcceptsVars(order.RequestedIso, order.OfferedIso, order.Protocol), nil;
}

/*func (r *p2pRules) CouldForward(order *Order) bool {
    //ToDo return r.acceptedChannels.
    return true
}*/

func (r *p2pRules) ShouldBan() bool {
    return r.banScore > 100
}

//-------------

type ordersState struct {
    NodeId      string
    Signature   []byte
    OrderIds    []string
    Timestamp   uint
}

func TakeOrdersStateSnapshot() *ordersState {
    //...ToDo
    return nil
}