package exchange

import (
    "github.com/Skyhark-Projects/dex/event"
    "github.com/Skyhark-Projects/dex/common"
)

type HistoryItem struct {
    IdentityHash    common.Hash
    Timestamp       uint
    Market          string
    Quantity        float64
    Rate            float64
    Buy             bool
}

var (
    OnHistory = new(event.Feed)
)