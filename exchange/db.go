package exchange

import (
    "github.com/Skyhark-Projects/dex/ethdb"
    "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/node"
    "github.com/Skyhark-Projects/dex/common"
)

type ExchangeDb struct {
    openOrders      *ethdb.LDBDatabase
    allOrders       *ethdb.LDBDatabase
    indexTimestamp  *ethdb.LDBDatabase
    indexIdentities *ethdb.LDBDatabase
    indexMarkets    *ethdb.LDBDatabase
}

func newExchangeDatabase(ctx *node.ServiceContext) (*ExchangeDb) {
    db := &ExchangeDb{};

    lvldb, err := ethdb.NewLDBDatabase(ctx.ResolvePath("orders"), 0, 0);
    db.allOrders = lvldb;
    if err != nil {
        log.Error("Could not initial levedb for orders service", "error", err)
        return nil
    }

    lvldb, err = ethdb.NewLDBDatabase(ctx.ResolvePath("orders/index-open"), 0, 0);
    db.openOrders = lvldb;
    if err != nil {
        log.Error("Could not initial levedb for open orders service", "error", err)
        return nil
    }

    lvldb, err = ethdb.NewLDBDatabase(ctx.ResolvePath("orders/index-timestamp"), 0, 0);
    db.indexTimestamp = lvldb;
    if err != nil {
        log.Error("Could not initial levedb for orders timestamp indexing service", "error", err)
        return nil
    }

    lvldb, err = ethdb.NewLDBDatabase(ctx.ResolvePath("orders/index-kyc"), 0, 0);
    db.indexIdentities = lvldb;
    if err != nil {
        log.Error("Could not initial levedb for orders indentity indexing service", "error", err)
        return nil
    }

    lvldb, err = ethdb.NewLDBDatabase(ctx.ResolvePath("orders/index-markets"), 0, 0);
    db.indexMarkets = lvldb;
    if err != nil {
        log.Error("Could not initial levedb for market orders indexing service", "error", err)
        return nil
    }

    return db
}

func (db *ExchangeDb) Put(id common.Hash) {
    
}

func (db *ExchangeDb) Get(id common.Hash) {
    
}

func (db *ExchangeDb) GetByIdentity(id common.Hash, market ...string) {
    //IF market len == 0 => return all orders of id
    //Else return orders where market in list (market standardize alphabetic name, keys = 'userid_market_timestamp_0')
}

func (db *ExchangeDb) GetByMarket(market string) {
    
}

func (db *ExchangeDb) GetByTimestamp(start uint, end ...uint) {
    
}