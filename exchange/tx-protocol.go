package exchange

import (
    "github.com/Skyhark-Projects/dex/common"
)

type TransactionProtocol interface {
    Name() string
    VerifyFunds(order *Order, spendSumOfIds ...map[common.Hash]map[string]uint) (bool, error);
    //LockFunds(order *Order) (bool, error);
    //CreateTransaction(..)
}

type GenericTxProtocol struct {
    
}

func (g GenericTxProtocol) Name() string {
    return "Generic"
}

func (g GenericTxProtocol) VerifyFunds(order *Order, spendSumOfIds ...map[common.Hash]map[string]uint) (bool, error) {
    return false, nil
}