package exchange

import (
	"github.com/Skyhark-Projects/dex/event"
	// "github.com/Skyhark-Projects/dex/log"
	// "fmt"
)

var (
    OnMatch = new(event.Feed)
)

type MatchItem struct {
    Order *Order
    Routes map[string] uint64
    ReceivedQuantity uint
		SendQuantity uint
}

type Match struct {
    Items []MatchItem
    FilledQuantity uint
}

type MatchEvent struct {
    *Match
    Order *Order
}

func (item *MatchItem) PackABI() ([]byte, error) {
    order, err := item.Order.PackABI(item.ReceivedQuantity);
    if(err != nil) {
        return order, err;
    }

    send, err := orderEndArguments.Pack(uint32(item.SendQuantity));
    if(err != nil) {
        return []byte{}, err;
    }

    packer := AbiPacker{
        abiItem{
            Content: order,
            Placeholder: true,
        },
        abiItem{
            Content: send,
            Placeholder: false,
        },
    };

    return packer.Flatten(), nil;
}

func (match *Match) PackABI() ([]byte, error) {
    res, err := orderEndArguments.Pack(uint32(len(match.Items)));
    if(err != nil) {
        return []byte{}, err;
    }

    packer := AbiPacker{};

    for _, elm := range match.Items {
        bts, err := elm.PackABI();

        if(err != nil) {
            return []byte{}, err;
        }

        packer = append(packer, abiItem{
            Content: bts,
            Placeholder: true,
        });
    }

    return append(res, packer.Flatten()...), nil;
}

func (match *Match) GetSpendQuantity() (uint) {
    var spend uint = 0;

    for _, elm := range match.Items {
        spend += elm.SendQuantity;
    }

    return spend;
}

func (match *Match) GetReceivedQuantity() (uint) {
    var recv uint = 0;

    for _, elm := range match.Items {
        recv += elm.ReceivedQuantity;
    }

    return recv;
}

func (match *Match) GetRate() (float64) {
	return float64(match.GetSpendQuantity()) / float64(match.GetReceivedQuantity())
}

//----------------------------------------------

func FindMatch(order *Order) *Match {
    res := Match{
        FilledQuantity: 0,
    }

    mainLint := GetRequestLints(order.OfferedToken.Iso)
    lintItem, oke := mainLint.Get(order.RequestedToken.Iso)

    if (!oke) {
			return &res
    }
		
    revRate := order.RevRate();
    for(lintItem != nil) {
        if(lintItem.Rate >= revRate) {
            match, err := order.GetMatch(lintItem.Current, &res);

            if(err == nil) {
                    // market := order.RequestedToken.Iso + "-" + order.OfferedToken.Iso
                    // if (market == "USDT-BTC") {
                    // 	log.Info("Matcher", "Market", market, "Rate", fmt.Sprintf("%.8f", float64(lintItem.Rate)), "LintItem", lintItem)
                    // }
                res.Items = append(res.Items, *match);
                res.FilledQuantity += match.ReceivedQuantity;

                if (res.FilledQuantity >= order.RequestedToken.Quantity) {
                    break;
                }
            }
        }

        lintItem = lintItem.Next
		}
		

    return &res
}

func HandleOrder(order *Order, allowBroadcast bool) (*Match, error) {
    /*match := FindMatch(order);

    if match.FilledQuantity > 0 {
        OnMatch.Send(&MatchEvent{
            Order: order,
            Match: match,
        })
    }

    order.RequestedToken.Quantity -= match.FilledQuantity;
    order.OfferedToken.Quantity -= match.GetSpendQuantity();

    if(allowBroadcast && match.FilledQuantity < order.RequestedToken.Quantity) {*/

        //ToDo broadcast order to network
        err := AppendOrder(order);

        /*if err != nil {
            return match, err;
        }
    }
    
    return match, nil*/
    return nil, err
}