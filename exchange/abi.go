package exchange;

import (
  "strings"
  "reflect"
  "errors"
  "encoding/binary"
  "github.com/Skyhark-Projects/dex/log"
  "github.com/Skyhark-Projects/dex/accounts/abi"
)

var orderTokenArguments = abi.Arguments{
    abi.Argument{
        Name: "Quantity",
        Type: getType("uint32"),
        Indexed: false,
    },
    abi.Argument{
        Name: "Iso",
        Type: getType("string"),
        Indexed: false,
    },
    abi.Argument{
        Name: "Address",
        Type: getType("string"),
        Indexed: false,
    },
}

var orderMidArguments = abi.Arguments{
    abi.Argument{
        Name: "Timestamp",
        Type: getType("int64"),
        Indexed: false,
    },
    abi.Argument{
        Name: "Ttl",
        Type: getType("uint32"),
        Indexed: false,
    },
    abi.Argument{
        Name: "StrictIdentity",
        Type: getType("bool"),
        Indexed: false,
    },
}

var orderEndArguments = abi.Arguments{
    abi.Argument{
        Name: "MatchedQuantity",
        Type: getType("uint32"),
        Indexed: false,
    },
}

//------------------

type abiItem struct {
    Content[] byte
    Placeholder bool
}

type AbiPacker []abiItem;

func (item *abiItem) GetArgumentSize() int {
    if(item.Placeholder) {
        return 32;
    }

    return len(item.Content);
}

func (item *abiItem) GetContentSize() int {
    return len(item.Content);
}

func (item *abiItem) ComputePosition(pos int) []byte {
    bts, _ := orderEndArguments.Pack(uint32(pos));
    return bts;
}

func (packer AbiPacker) GetArgumentsLength() int {
    size := 0;
    for _, element := range packer {
        size = size + element.GetArgumentSize();
    }

    return size;
}

func (packer AbiPacker) Flatten() []byte {
    res   := []byte{};
    after := []byte{};
    argSize := packer.GetArgumentsLength();

    for _, element := range packer {
        if(element.Placeholder) {
            pos := element.ComputePosition( argSize + len(after) );
            res = append(res, pos...);
            after = append(after, element.Content...);
        } else {
            res = append(res, element.Content...);
        }
    }

    return append(res, after...);
}

//------------------

func PushArgument(Args abi.Arguments, Name string, Type string) abi.Arguments {
    t, err := abi.NewType(Type);
    
    if(err != nil) {
        log.Error("Type Create", Type, err)
    }

    arg := abi.Argument{
        Name: Name,
        Type: t,
        Indexed: false,
    }

    return append(Args, arg);
}

func parseTypeName(name string) string {
    index := strings.Index(name, "[]");
    appendArray := false;
    
    if(index == 0) {
        name = name[2:];
        appendArray = true;
    }
    
    if(name == "uint") {
        name = "uint256";
    } else if(name == "*big.Int") {
        name = "int64";
    }
    
    if(appendArray) {
        return name + "[]";
    }
    
    return name
}

func GetAbiArguments(r interface{}) abi.Arguments {
    Args := abi.Arguments{}
    
    if(r == nil) {
        return Args;
    }

    s := reflect.ValueOf(r).Elem()
    typeOfT := s.Type()

    for i := 0; i < s.NumField(); i++ {
        f := s.Field(i)
        Args = PushArgument(Args, typeOfT.Field(i).Name, parseTypeName(f.Type().String()));
    }

    return Args;
}

/*func PackAbi(r interface{}) ([]byte, error) {
    Args := GetAbiArguments(r);
    
    return (nil, nil);
    //..return Args.Pack(r.Test, r.Test2);
}*/

func UnpackAbi(r interface{}, result[] byte) error {
    isStructResult := true;
    
    if(32 <= len(result)) {
        for i := 0; i < 32; i++ {
            if((i == 31 && result[i] != 32) || (i < 31 && result[i] != 0)) {
                isStructResult = false;
                break;
            }
        }
    } else {
       isStructResult = false; 
    }
    
    if(isStructResult) {
        result = result[32:];
    }

    Args := GetAbiArguments(r);
    return Args.Unpack(r, result);
}

//------------------

func getType(name string) abi.Type {
    t, _ := abi.NewType(name);
    return t;
}

//------------------

func PackSmartContractOrders(inputOrder *Order, inputQuantity uint, outputOrder *Order, outputQuantity uint, match *Match) ([]byte, error) {
    inputBytes, err := inputOrder.PackABI(inputQuantity);
    if(err != nil) {
        return inputBytes, err;
    }

    outputBytes, err := outputOrder.PackABI(outputQuantity);
    if(err != nil) {
        return inputBytes, err;
    }

    matchBytes, err := match.PackABI();
    if(err != nil) {
        return inputBytes, err;
    }

    packer := AbiPacker{
        abiItem{
            Content: inputBytes,
            Placeholder: true,
        },
        abiItem{
            Content: outputBytes,
            Placeholder: true,
        },
        abiItem{
            Content: matchBytes,
            Placeholder: true,
        },
    };

    return packer.Flatten(), nil;
}

type contractResult struct {
    Matched bool;
    Routes map[string] uint64;
    Data []byte;
    Gas uint64;
}

type contractResultParser struct {
    Matched bool;
    RoutesPosition uint32;
    ValuesPosition uint32;
    RoutesLength uint32;
}

func ParseContractResult(data []byte, gas ...uint64) (*contractResult, error) {
    res := contractResult{
        Matched: false,
        Routes: map[string]uint64{},
        Data: data,
        Gas: 0,
    };
    
    if(len(gas) > 0) {
        res.Gas = gas[0];
    }

    args := abi.Arguments{
        abi.Argument{
            Name: "Matched",
            Type: getType("bool"),
            Indexed: false,
        },
        abi.Argument{
            Name: "RoutesPosition",
            Type: getType("uint32"),
            Indexed: false,
        },
        abi.Argument{
            Name: "ValuesPosition",
            Type: getType("uint32"),
            Indexed: false,
        },
        abi.Argument{
            Name: "RoutesLength",
            Type: getType("uint32"),
            Indexed: false,
        },
    }

    //Parse arr positions
    info := contractResultParser{};
    err := args.Unpack(&info, data);
    res.Matched = info.Matched;
    info.RoutesPosition += 32;
    info.ValuesPosition += 32;

    if(err != nil) {
        return &res, err;
    } else if(info.ValuesPosition + (info.RoutesLength * 32) > uint32(len(data))) {
        return &res, errors.New("Missing route values");
    }

    //Parse routes
    addrPos := int(info.RoutesPosition);
    valPos  := int(info.ValuesPosition);

    for i := 0; i < int(info.RoutesLength); i++ {
        addr := string(data[addrPos: addrPos + 32]);
        val  := binary.BigEndian.Uint64( data[valPos + 24: valPos + 32] );

        if val2, ok := res.Routes[addr]; ok {
            res.Routes[addr] = val + val2;
        } else {
            res.Routes[addr] = val;
        }

        addrPos += 32;
        valPos += 32;
	}

    return &res, nil;
}

func (res *contractResult) GetRoutedValue(addr ...string) uint64 {
    if(len(addr) != 0) {
        if val, ok := res.Routes[addr[0]]; ok {
            return val;
        }
    }

    var val uint64 = 0;
    for _, item := range res.Routes {
        val += item;
    }

    return val;
}