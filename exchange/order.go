package exchange

import (
		// "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/kyc"
    "github.com/Skyhark-Projects/dex/exchange/validators/btc"
    "github.com/Skyhark-Projects/dex/rlp"
    "github.com/Skyhark-Projects/dex/common"
    "github.com/Skyhark-Projects/dex/core/state"
    "github.com/Skyhark-Projects/dex/core/vm"
    "github.com/Skyhark-Projects/dex/core/vm/runtime"
    "github.com/Skyhark-Projects/dex/ethdb"
    "io"
    "strings"
    "time"
    "errors"
    "bytes"
    "bufio"
    "strconv"
    "math"
    "math/big"
    "crypto/sha256"
    "sync/atomic"
    "sync"
)

type orderRlp struct {
    Identity        kyc.IdentitySignature   `json:"from"       gencodec:"required"`
    Timestamp       uint                    `json:"time"       gencodec:"required"`
    StrictIdentity  bool                    `json:"trusted"    gencodec:"required"`
    RequestedToken  OrderToken              `json:"request"    gencodec:"required"`
    OfferedToken    OrderToken              `json:"offer"      gencodec:"required"`
    Ttl             *uint                   `json:"ttl"        rlp:"nil"`
    Contract        []byte                  `json:"contract"   gencodec:"required"`
    LockTime        uint                    `json:"lock"       gencodec:"required"`
    StateId         uint                    `json:"state"      gencodec:"required"` //incremented in order to update an existing order
    Sequenze        uint                    `json:"sequenze"   gencodec:"required"` //incremented in order to avoid duplicate order ids
    //protocolName    string
}

type OrderIdRlp struct {
    Identity        common.Hash
    Timestamp       uint
    StrictIdentity  bool
    RequestedIso    string
    RequestedAddr   string
    OfferedIso      string
    OffreredAddr    string
    Ttl             *uint
    Contract        []byte
    Protocol        string
    Sequenze        uint
}

type orderSignRlp struct {
    Id              string
    RequestQuantity uint
    OfferQuantity   uint
    StateId         uint
}

type Order struct {
    id atomic.Value `json:"id"       rlp:"nil"`
    orderRlp
    Protocol TransactionProtocol
}

type orderSequenze struct {
    value uint
    mutex sync.Mutex
}

type orderTokenRLP struct {
    Quantity        uint    `json:"quantity"    gencodec:"required"` //satochis
    Iso             string  `json:"iso"         gencodec:"required"`
    Address         string  `json:"address"     rlp:"nil"`
}

type OrderToken struct {
    orderTokenRLP
}

func ValidateOrderToken(token OrderToken) (ok bool, err error) {
    //ToDo add ERC20 token list to check ethereum address

    switch(token.Iso) {
        case "ETH":
            if common.IsHexAddress(token.Address) {
                return true, nil
            }

            return false, errors.New("Invalid Ethereum address")
        case "BTC":
            return btc.ValidAddress(token.Address)
        //case "BCH":
        //    return btc.ValidAddress(token.Address) 
    }

    return true, nil
}

func NewOrderToken(quantity uint, iso string, address ...string) (rt *OrderToken, err error) {
    token := OrderToken{}
    token.Quantity = quantity
    token.Iso = strings.ToUpper(iso)

    if(token.Iso == "BCC" || token.Iso == "BCHABC") {
        token.Iso = "BCH"
    } else if(token.Iso == "BSV") {
        token.Iso = "BCHSV"
    }

    if len(address) > 0 && address[0] != "" {
        token.Address = address[0]

        ok, err := ValidateOrderToken(token)

        if(!ok) {
            if err == nil {
                return nil, errors.New("Unknown token request/offer creation error");
            }

            return nil, err
        }
    }
    
    return &token, nil
}

var (
    sequenze = orderSequenze{ value: 0 }
)

func New() *Order {
    sequenze.mutex.Lock()
    defer sequenze.mutex.Unlock()
    
    order := Order{
        orderRlp: orderRlp{
            Timestamp:      uint(time.Now().UnixNano() / int64(time.Millisecond)),
            StrictIdentity: false,
            LockTime:       0,
            StateId:        0,
            Sequenze:       sequenze.value,
        },
        Protocol:       GenericTxProtocol{},
    }

    sequenze.value++;
    if sequenze.value > 100000000 {
        sequenze.value = 0
    }

    return &order
}

//---------------

func (o *Order) Id() string {
    if id := o.id.Load(); id != nil {
        return id.(string)
    }

    var b bytes.Buffer;
    writer := bufio.NewWriter(&b);
    err := rlp.Encode(writer, &OrderIdRlp{
        Identity        : o.Identity.IdentityHash,
        Timestamp       : o.Timestamp,
        StrictIdentity  : o.StrictIdentity,
        RequestedIso    : o.RequestedToken.Iso,
        RequestedAddr   : o.RequestedToken.Address,
        OfferedIso      : o.OfferedToken.Iso,
        OffreredAddr    : o.OfferedToken.Address,
        Ttl             : o.Ttl,
        Contract        : o.Contract,
        Protocol        : o.Protocol.Name(),
        Sequenze        : o.Sequenze,
    })

    if(err != nil) {
        return "Error";
    }

    writer.Flush();
    h := sha256.New();
    h.Write(b.Bytes());
    id := common.Bytes2Hex(h.Sum(nil));

    o.id.Store(id)
    return id
}

func (o *Order) Request(quantity uint, iso string, address string) (rt *OrderToken, err error) {
    rt, err = NewOrderToken(quantity, iso, address)
    
    if(rt != nil) {
        o.RequestedToken = *rt
    }

    return rt, err
}

func (o *Order) Offer(quantity uint, iso string, address ...string) (token *OrderToken, err error) {
    if len(address) > 0 {
        token, err = NewOrderToken(quantity, iso, address[0])
    } else {
        token, err = NewOrderToken(quantity, iso)
    }

    if(token != nil) {
        o.OfferedToken = *token
    }

    return token, err
}

func (o *Order) SignedBytes() ([]byte, error) {
    var b bytes.Buffer;
    writer := bufio.NewWriter(&b)
    err := rlp.Encode(writer, orderSignRlp{
        Id:              o.Id(),
        RequestQuantity: o.RequestedToken.Quantity,
        OfferQuantity:   o.OfferedToken.Quantity,
        StateId:         o.StateId,
    })

    if err != nil {
        return nil, err
    }

    writer.Flush();
    return b.Bytes(), nil
}

func (o *Order) Sign(identity *kyc.Identity) error {
    if(identity.PrivateKey == nil) {
        return errors.New("The private key of the given identity is unkown, no signatures could be generated from this identity")
    }
    
    if(identity.Certificate == nil) {
        return errors.New("The certificate of the given identity is unkown, no signatures could be generated from this identity")
    }

    o.Identity = kyc.IdentitySignature{
        IdentityHash: identity.Id(),
    }

    /*bts, err := o.SignedBytes()
    if(err != nil) {
        return err
    }

    signature, err := identity.Sign(bts)
    if(err != nil) {
        return err
    }

    o.Identity.Signature = signature*/
    o.Identity.Signature = []byte("todo-reenable")
    return nil
}

func (o *Order) VerifySignature() bool {
    /*bts, err := o.SignedBytes()
    if(err != nil) {
        return false
    }

    return o.Identity.IsValidForBytes(bts);*/
    return true
}

func (o *Order) RequestVerifiedUser() {
    o.StrictIdentity = true
}

func (o *Order) SetTtl(ttl uint) {
    o.Ttl = &ttl
}

//---------------

func (o *Order) ExecuteContract(inputQuantity uint, outputOrder *Order, outputQuantity uint, match *Match) (*contractResult, error) {
    input, err := PackSmartContractOrders(o, inputQuantity, outputOrder, outputQuantity, match);

    if(err != nil) {
        return nil, err;
    }

    //Input = function execute(Order myOrder, Order matchedOrder, Match[] MatchedList) public returns(bool, bytes32[])
    input = append([]byte{ 106, 233, 106, 137 }, input...);

    var (
        tracer      vm.Tracer
        sender      = common.StringToAddress("AnonymeSender")
        receiver    = common.StringToAddress("AnonymeReceiver")
        ret         []byte
        leftOverGas uint64
    )

    senderIdentity := o.Identity.GetIdentity();
    if(senderIdentity != nil) {
        sender = common.StringToAddress(senderIdentity.GetToken())
    }

    receiverIdentity := outputOrder.Identity.GetIdentity();
    if(receiverIdentity != nil) {
        receiver = common.StringToAddress(receiverIdentity.GetToken())
    }

    //----------

    db, _      := ethdb.NewMemDatabase()
    statedb, _ := state.New(common.Hash{}, state.NewDatabase(db))

    statedb.CreateAccount(sender)

    runtimeConfig := runtime.Config{
    Origin:   sender,
    State:    statedb,
        GasLimit: uint64(1000000),
        GasPrice: big.NewInt(0),
        Value:    big.NewInt(0),
        EVMConfig: vm.Config{
          Tracer: tracer,
          Debug:  false,
        },
    }

    statedb.SetCode(receiver, o.Contract)
    ret, leftOverGas, err = runtime.Call(receiver, input, &runtimeConfig)

    if err != nil {
        return nil, err;
    }

    res, err := ParseContractResult(ret, runtimeConfig.GasLimit - leftOverGas);
    if err != nil {
        return res, err;
    }

    if(res.GetRoutedValue() > uint64(inputQuantity)) {
        var buffer bytes.Buffer;
        buffer.WriteString("Routed value (");
        buffer.WriteString(strconv.FormatUint(uint64(res.GetRoutedValue()), 10));
        buffer.WriteString(") bigger than the total matched value (");
        buffer.WriteString(strconv.FormatUint(uint64(inputQuantity), 10));
        buffer.WriteString(").");
        return res, errors.New(buffer.String());
    }

    return res, err;
}

func (a *Order) GetMatch(b *Order, match ...*Match) (*MatchItem, error) {
		
    //Verify matched rates
    if(a.Rate() < b.RevRate()) {
        return nil, errors.New("Rates mismatch");
    }

    //Verify matched ISO's
    if(a.RequestedToken.Iso != b.OfferedToken.Iso || a.OfferedToken.Iso != b.RequestedToken.Iso) {
        return nil, errors.New("Wrong market provided");
    }
    
    //Verify identities
    if(b.VerifySignature() == false) {
        return nil, errors.New("Matcher identity not valid");
    }

    if(a.VerifySignature() == false) {
        return nil, errors.New("Own identity not valid");
    }

    /*if(a.StrictIdentity && b.Identity.IsTrusted() == false) {
        return nil, errors.New("Matcher identity not trusted");
    }

    if(b.StrictIdentity && a.Identity.IsTrusted() == false) {
        return nil, errors.New("Own identity not trusted");
		}*/
		
    // Verify not own order
		/*
		if(a.Identity.GetToken() == b.Identity.GetToken()) {
				return nil, errors.New("Order is own order");
		}
		*/

    //Verify smart contracts
    var matchList *Match;
    if(len(match) > 0) {
        matchList = match[0];
    } else {
        matchList = &Match{};
    }

    //Create Match Item & Verify smart contracts
    recvQuantity := math.Min(float64(a.RequestedToken.Quantity - matchList.FilledQuantity), float64(b.OfferedToken.Quantity));
		sendQuantity := uint((recvQuantity / float64(b.OfferedToken.Quantity)) * float64(b.RequestedToken.Quantity))
		// sendQuantity2 := uint((float64(b.OfferedToken.Quantity) / float64(b.RequestedToken.Quantity)) * recvQuantity)

    matchResult := MatchItem{
        Order: b,
        Routes: map[string]uint64{},
        ReceivedQuantity: uint(recvQuantity),
				SendQuantity: sendQuantity,
    }

    if(len(a.Contract) > 0) {
        res, err := a.ExecuteContract(matchResult.SendQuantity, b, matchResult.ReceivedQuantity, matchList);

        if(res.Matched == false || err != nil) {
            return nil, err;
        }
    }

    if(len(b.Contract) > 0) {
        res, err := b.ExecuteContract(matchResult.ReceivedQuantity, a, matchResult.SendQuantity, matchList);

        if(res.Matched == false || err != nil) {
            return nil, err;
        }

        matchResult.Routes = res.Routes;
    }

    return &matchResult, nil;
}

func (o *Order) Rate() float64 {
    return float64(o.OfferedToken.Quantity) / float64(o.RequestedToken.Quantity)
}

func (o *Order) RevRate() float64 {
    return float64(o.RequestedToken.Quantity) / float64(o.OfferedToken.Quantity)
}

func (o *Order) PackABI(matchedQuantity uint, trusted ...bool) ([]byte, error) {
    idBytes, err := o.Identity.PackABI(trusted...);
    if(err != nil) {
        return []byte{}, err;
    }

    reqBytes, err := o.RequestedToken.PackABI();
    if(err != nil) {
        return []byte{}, err;
    }

    offBytes, err := o.OfferedToken.PackABI();
    if(err != nil) {
        return []byte{}, err;
    }

    var ttl uint32;
    if(o.Ttl != nil) {
        ttl = uint32(*o.Ttl);
    } else {
        ttl = 0;
    }
    
    mid, err := orderMidArguments.Pack(o.Timestamp, ttl, o.StrictIdentity);
    if(err != nil) {
        return []byte{}, err;
    }

    end, err := orderEndArguments.Pack(uint32(matchedQuantity));
    if(err != nil) {
        return []byte{}, err;
    }

    packer := AbiPacker{
        abiItem{
            Content: idBytes,
            Placeholder: true,
        },
        abiItem{
            Content: mid,
            Placeholder: false,
        },
        abiItem{
            Content: reqBytes,
            Placeholder: true,
        },
        abiItem{
            Content: offBytes,
            Placeholder: true,
        },
        abiItem{
            Content: end,
            Placeholder: false,
        },
    };

    return packer.Flatten(), nil;
}

//---------------

func (o *Order) CreateRemoveLog(key ...string) (*OrderRemoveLog, error) {
    id := o.Identity.GetIdentity()
    oldKey := id.PrivateKey

    if len(key) > 0 {
        err := id.ParsePrivateKey(key[0])
        if err != nil {
            return nil, err
        }
    }

    if id.PrivateKey == nil {
        return nil, errors.New("Unknown identity private key")
    }
    
    binlog := OrderRemoveLog{
        Identity:       kyc.IdentitySignature{},
        OrderId:        o.Id(),
        RequestedIso:   o.RequestedToken.Iso,
        OfferedIso:     o.OfferedToken.Iso,
        Protocol:       o.Protocol.Name(),
    }

    err := binlog.Sign(id)
    if err != nil {
        return nil, err
    }

    if oldKey != nil {
        id.PrivateKey = oldKey;
    }

    return &binlog, nil
}

func (o *Order) Remove(key ...string) (*OrderRemoveLog, error) {
    log, err := o.CreateRemoveLog(key...)

    if err != nil {
        return log, err
    }

    log.RemoveFromPool()
    return log, err
}

//---------------

type writeCounter common.StorageSize

func (c *writeCounter) Write(b []byte) (int, error) {
	*c += writeCounter(len(b))
	return len(b), nil
}

// Size returns the true RLP encoded storage size of the OrderToken
func (o *OrderToken) Size() common.StorageSize {
	c := writeCounter(0)
    o.EncodeRLP(&c)
	return common.StorageSize(c)
}

func (b *OrderToken) EncodeRLP(w io.Writer) error {
    return rlp.Encode(w, orderTokenRLP{
        Quantity: b.Quantity,
        Iso: b.Iso,
        Address: b.Address,
    });
}

func (b *OrderToken) DecodeRLP(s *rlp.Stream) error {
    o := orderTokenRLP{}
    err := s.Decode(&o)

    if err != nil {
        return err;
    }

    b.Quantity = o.Quantity;
    b.Iso = o.Iso;
    b.Address = o.Address;
    return err
}

func (b *OrderToken) PackABI() ([]byte, error) {
    return orderTokenArguments.Pack(uint32(b.Quantity), b.Iso, b.Address);
}

//---------------

// EncodeRLP serializes b into the Ethereum RLP block format.
func (b *Order) EncodeRLP(w io.Writer) error {
    return rlp.Encode(w, &orderRlp{
        Identity: b.Identity,
        Timestamp: b.Timestamp,
        StrictIdentity: b.StrictIdentity,
        RequestedToken: b.RequestedToken,
        OfferedToken:  b.OfferedToken,
        Ttl: b.Ttl,
        Contract: b.Contract,
        LockTime: b.LockTime,
        StateId: b.StateId,
        Sequenze: b.Sequenze,
    })
}

func (b *Order) DecodeRLP(s *rlp.Stream) error {
    o := orderRlp{}
    err := s.Decode(&o)

    if err != nil {
        return err;
    }

    b.Protocol = GenericTxProtocol{}
    b.Identity = o.Identity;
    b.Timestamp = o.Timestamp;
    b.StrictIdentity = o.StrictIdentity;
    b.RequestedToken = o.RequestedToken;
    b.OfferedToken =  o.OfferedToken;
    b.Ttl = o.Ttl;
    b.Contract = o.Contract;
    b.LockTime = o.LockTime;
    b.StateId = o.StateId;
    b.Sequenze = o.Sequenze;
    return err
}