package exchange

import (
    "github.com/Skyhark-Projects/dex/kyc"
    "github.com/Skyhark-Projects/dex/common"
    "github.com/Skyhark-Projects/dex/rlp"
    "github.com/Skyhark-Projects/dex/p2p/discover"
    //"github.com/Skyhark-Projects/dex/log"
    "bytes"
    "bufio"
    "errors"
)

type OrderRemoveLog struct {
    Identity     kyc.IdentitySignature
    OrderId      string
    RequestedIso string
    OfferedIso   string
    Protocol     string
}

type orderRemoveSignatureRlp struct {
    Identity common.Hash
    OrderId string
    RequestedIso string
    OfferedIso string
    Protocol string
}

func (bin *OrderRemoveLog) SerializeSignatureRLP() ([]byte, error) {
    var b bytes.Buffer;
    writer := bufio.NewWriter(&b)
    err := rlp.Encode(writer, orderRemoveSignatureRlp{
        Identity:   bin.Identity.IdentityHash,
        OrderId:    bin.OrderId,
        RequestedIso: bin.RequestedIso,
        OfferedIso:   bin.OfferedIso,
        Protocol:     bin.Protocol,
    })

    if err != nil {
        return nil, err
    }

    writer.Flush();
    return b.Bytes(), nil
}

func (bin *OrderRemoveLog) VerifySignature() bool {
    input, err := bin.SerializeSignatureRLP()
    if err != nil {
        return false
    }

    return bin.Identity.IsValidForBytes(input)
}

func removeOrderAtPosition(main lintOfferList, lintItem *lint) {
    if(lintItem.Prev == nil) {
        main.Put(lintItem.Current.OfferedToken.Iso, lintItem.Next)
    } else {
        lintItem.Prev.Next = lintItem.Next
    }

    if(lintItem.Next != nil) {
        lintItem.Next.Prev = lintItem.Prev
    }
}

func (bin *OrderRemoveLog) RemoveFromPool(ignoredPeers ...discover.NodeID) (bool, error) {
    if(bin.VerifySignature() == false) {
        return false, errors.New("Invalid signature");
    }

    lints.RLock()
    main, ok := lints[bin.RequestedIso]
    lints.RUnlock()
    
    if(ok == false) {
        return false, nil
    }

    lintItem, ok := main.Get(bin.OfferedIso)
    if(ok == false || lintItem == nil) {
        return  false, nil
    }

    for(lintItem != nil) {
        if(lintItem.Current.Id() == bin.OrderId) {
            if(bin.Identity.IdentityHash != lintItem.Current.Identity.IdentityHash) {
                return false, errors.New("Invalid identity")
            }

            if(bin.Protocol != lintItem.Current.Protocol.Name()) {
                return false, errors.New("Invalid protocol")
            }

            main.LockOffer(lintItem.Current.OfferedToken.Iso)
            removeOrderAtPosition(main, lintItem)
            main.UnlockOffer(lintItem.Current.OfferedToken.Iso)

            PeersPool.RemoveOrder(bin, ignoredPeers...)
            return true, nil
        }

        lintItem = lintItem.Next
    }

    //log.Error("RemoveFromPool", "test", "remove from pool done")
    return false, nil
}

func (bin *OrderRemoveLog) Sign(id *kyc.Identity) error {
    bin.Identity.IdentityHash = id.Id()
    
    bts, err := bin.SerializeSignatureRLP()
    if err != nil {
        return err
    }

    signature, err := id.Sign(bts)
    if err != nil {
        return err
    }

    bin.Identity.Signature = signature
    return nil
}
