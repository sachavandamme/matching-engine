package exchange

import (
    "github.com/Skyhark-Projects/dex/rpc"
    "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/node"
    "github.com/Skyhark-Projects/dex/p2p"
    "github.com/Skyhark-Projects/dex/p2p/discover"
    "github.com/Skyhark-Projects/dex/kyc"
    "github.com/Skyhark-Projects/dex/common/hexutil"
    "strings"
    "errors"
    "time"
    "math/rand"
    "fmt"
    "runtime"
)

type RPCService struct {
    service     *exchangeService
    dbOrders    *ExchangeDb
}

func (s *RPCService) CreateOrder(request *OrderToken, offer *OrderToken, verifyIdentity bool, idName string) (string, error) {
    order := New()

    _, err := order.Request(request.Quantity, request.Iso, request.Address)
    if(err != nil) {
        return "", err
    }

    _, err = order.Offer(offer.Quantity, offer.Iso, offer.Address)
    if(err != nil) {
        return "", err
    }

    if(verifyIdentity) {
        order.RequestVerifiedUser()
    }

    id := kyc.GetOwnIdentityByToken(idName);
    if id == nil {
        return "", errors.New("Identity not found");
    }

    if err := order.Sign(id); err != nil {
        return "", err
    }

    get := s.GetOrder(order.Id())
    _, err = HandleOrder(order, true)
    if err != nil {
        //log.Error("error", "order", order.Id(), "get", get.Id(), "sequenze", order.Sequenze, "seq2", get.Sequenze)
        log.Error("error", "order", order, "get", get)
        return "", err
    }

    return order.Id(), nil
}

func (s *RPCService) UpdateOrder(id string, request *OrderToken, offer *OrderToken) (bool, error) {
    order := GetOrder(id, request.Iso, offer.Iso)
    if order == nil {
        return false, errors.New("Update Error: The requested order could not be found")
    }

    order2 := &Order{
        orderRlp: order.orderRlp,
        Protocol: order.Protocol,
        id:       order.id,
    }

    _, err := order2.Request(request.Quantity, request.Iso, order.RequestedToken.Address)
    if(err != nil) {
        return false, err
    }

    _, err = order2.Offer(offer.Quantity, offer.Iso, order.OfferedToken.Address)
    if(err != nil) {
        return false, err
    }

    identity := order.Identity.GetIdentity()
    if identity == nil {
        return false, errors.New("Identity not found");
    }

    order2.StateId = order.StateId+1
    if err := order2.Sign(identity); err != nil {
        return false, err
    }

    _, err = HandleOrder(order2, true)
    if err != nil {
        return false, err
    }

    return true, nil
}

func (s *RPCService) StressTest(identity string, cicles int) (uint, error) {
    id := kyc.GetOwnIdentityByToken(identity);
    if id == nil {
        return 0, errors.New("Identity not found");
    }

    start := time.Now().UnixNano()
    for i := 0; i < cicles; i++ {
        order := New()
        //order.Contract = getContract();
        order.Request(4, "btc", "1CK6KHY6MHgYvmRQ4PAafKYDrg1ejbH1cE")
        order.Offer(uint(rand.Uint32()), "bch", "1CK6KHY6MHgYvmRQ4PAafKYDrg1ejbH1cE")
        order.RequestVerifiedUser()
        order.Sign(id)

        AppendOrder(order)
    }

    end := (time.Now().UnixNano() - start)
    return uint(end / int64(time.Millisecond)), nil
}

func (s *RPCService) GetOrder(id string) *Order {
    lints.RLock()
    defer lints.RUnlock()

    for _, main := range lints {
        main.RLock()
        for _, lint := range main.offerLints {
            item := lint

            for item != nil {
                if(item.Current.Id() == id) {
                    main.RUnlock()
                    return item.Current
                }
                item = item.Next
            }
        }
        
        main.RUnlock()
    }

    return nil
}

func (s *RPCService) RemoveOrder(id string, market string) (bool, error) {
    markets := strings.Split(strings.ToUpper(market), "-")

    if(len(markets) != 2) {
        return false, errors.New("Wrong market name provided")
    }

    order := GetOrder(id, markets[1], markets[0])
    if(order == nil) {
        order = GetOrder(id, markets[0], markets[1])
    }

    if(order == nil) {
        return false, errors.New("Order not found")
    }

    _, err := order.Remove()
    if(err != nil) {
        return false, err
    }

    return true, nil
}

func RemoveAllOrdersOfToken(idName string) (uint, error) {
    var count uint = 0

    for _, main := range lints {
        main.RLock()
        for _, lint := range main.offerLints {
            item := lint

            for item != nil {
                if(item.Current.Identity.GetToken() == idName) {
                    main.RUnlock()

                    if _, err := item.Current.Remove(); err != nil {
                        return count, err
                    }

                    main.RLock()
                    count++
                }
                item = item.Next
            }
        }
        main.RUnlock()
    }

    return count, nil
}

func RemoveOwnOrdersFor(idName string, reqIso string, offIso string) (uint, error) {
    var count uint = 0

    main := lints[reqIso]
    main.RLock()

    item := main.offerLints[offIso]

    for item != nil {
        if(item.Current.Identity.GetToken() == idName) {
            main.RUnlock()

            if _, err := item.Current.Remove(); err != nil {
                return count, err
            }

            main.RLock()
            count++
        }
        item = item.Next
    }

    main.RUnlock()
    return count, nil
}

func RemoveOwnOrdersForMarket(idName string, market string) (uint, error) {
    markets := strings.Split(strings.ToUpper(market), "-")

    c1, err := RemoveOwnOrdersFor(idName, markets[0], markets[1])
    c2, err2 := RemoveOwnOrdersFor(idName, markets[0], markets[1])
    if err != nil {
        return c1 + c2, err
    }

    return c1 + c2, err2
}


func (s *RPCService) RemoveAllOrders(idName string) (uint, error) {
    return RemoveAllOrdersOfToken(idName)
}

func (s *RPCService) GetOrders(idName string) ([]string, error) {
    lints.RLock()
    defer lints.RUnlock()
    res := []string{}

    for _, main := range lints {
        main.RLock()
        for _, lint := range main.offerLints {
            item := lint

            for item != nil {
                if(item.Current.Identity.GetToken() == idName) {
                    res = append(res, item.Current.Id())
                }
                item = item.Next
            }
        }
        
        main.RUnlock()
    }

    return res, nil
}

func (s *RPCService) OrderBook(market string, limit uint) (*FlatOrderBook, error) {
    markets := strings.Split(strings.ToUpper(market), "-")

    if(len(markets) != 2) {
        return nil, errors.New("Wrong market name provided")
    }

    orderBook := GetOrderBook(markets[1], markets[0])
    return orderBook.Flatten(false, limit), nil
}

func (s *RPCService) OrderBookWithTimeAgo(market string, limit uint) (*FlatOrderBook, error) {
    markets := strings.Split(strings.ToUpper(market), "-")

    if(len(markets) != 2) {
        return nil, errors.New("Wrong market name provided")
    }

    orderBook := GetOrderBook(markets[1], markets[0])
    return orderBook.Flatten(true, limit), nil
}

func (s *RPCService) RawOrderBook(market string) (*RawFlatOrderBook, error) {
    markets := strings.Split(strings.ToUpper(market), "-")

    if(len(markets) != 2) {
        return nil, errors.New("Wrong market name provided")
    }

    orderBook := GetOrderBook(markets[1], markets[0])
    return orderBook.RawFlatten(), nil
}

func (s *RPCService) PeerCount() hexutil.Uint {
	server := s.service.server
	return hexutil.Uint(server.PeerCount())
}

func (s *RPCService) PeerCaps(id string) []string {
	peers := s.service.server.Peers()
	var arr []string
	for _, peer := range peers {
		if (peer.ID().String() == id) {
			for _, cap := range peer.Caps() {
				arr = append(arr, cap.String())
			}
		}
	}
	return arr
}

func (s *RPCService) PeerLoad(id string) []string {
	peers := s.service.server.Peers()
	var arr []string
	for _, peer := range peers {
		if (peer.ID().String() == id) {
			arr = peer.LoadArray();
		}
	}
	return arr
}

// Todo start / limit
func (s *RPCService) PeersRemote() []string {
		peers := s.service.server.Peers()
		var arr []string
		for _, peer := range peers {
			arr = append(arr, peer.RemoteAddr().String())
		}
    return arr
}

// Todo start / limit
func (s *RPCService) PeersLocal() []string {
	peers := s.service.server.Peers()
	var arr []string
	for _, peer := range peers {
		arr = append(arr, peer.LocalAddr().String())
	}
	return arr
}

// Todo start / limit
func (s *RPCService) PeersIds() []string {
	peers := s.service.server.Peers()
	var arr []string
	for _, peer := range peers {
		arr = append(arr, peer.ID().String())
	}
	return arr
}

func (s *RPCService) NetAddress() string {
		server := s.service.server
    return server.Self().String()
}

func (s *RPCService) NetId() string {
    server := s.service.server
    return server.Self().ID.String()
}

func (s *RPCService) NetUDP() string {
    server := s.service.server
    return fmt.Sprint(server.Self().UDP)
}

func (s *RPCService) NetTCP() string {
    server := s.service.server
    return fmt.Sprint(server.Self().TCP)
}

func (s *RPCService) NetCaps() []string {
	server := s.service.server
	var arr []string
	for _, proto := range server.Protocols {
		version := fmt.Sprint(proto.Version);
		arr = append(arr, (proto.Name+"/"+version))
	}
	return arr
}

func (s *RPCService) RuntimeStats() []string {
    var arr []string
    var m runtime.MemStats
    runtime.ReadMemStats(&m)
    arr = append(arr, "Alloc:"+fmt.Sprint(m.Alloc))
    arr = append(arr, "TotalAlloc:"+fmt.Sprint(m.TotalAlloc))
    arr = append(arr, "Sys:"+fmt.Sprint(m.Sys))
    arr = append(arr, "NumGC:"+fmt.Sprint(m.NumGC))
    return arr
}

func (s *RPCService) TxSpeed() []string {
	var arr []string
	arr = append(arr, rateCounterSec.String())
	arr = append(arr, rateCounterMin.String())
	return arr
}

func (s *RPCService) GetChannelsConfig() *P2PAcceptedChannels {
    return GetAcceptedChannals()
}

func (s *RPCService) AvailableMarkets() []string {
    var result []string

    lints.RLock()
    defer lints.RUnlock()

    for base, lint := range lints {
        lint.RLock()
        for market := range lint.offerLints {
            result = append(result, base + "-" + market)
        }
        lint.RUnlock()
    }

    return result
}



//------------

func ServiceRegistrer(name string) func(ctx *node.ServiceContext) (node.Service, error) {
    return func(nodeCtx *node.ServiceContext) (node.Service, error) {
        /*ctx := &ServiceContext{
            RPCDialer:   self.adapter,
            NodeContext: nodeCtx,
            Config:      self.config,
        }
        if snapshots != nil {
            ctx.Snapshot = snapshots[name]
        }
        serviceFunc := self.adapter.services[name]
        service, err := serviceFunc(ctx)
        if err != nil {
            return nil, err
        }
        self.running[name] = service
        return service, nil*/
        
        service := newExchangeService(nodeCtx)

        return service, nil
    }
}

type exchangeService struct {
    log      log.Logger
    server	 *p2p.Server
    ctx      *node.ServiceContext
}

func newExchangeService(ctx *node.ServiceContext) *exchangeService {
    service := &exchangeService{
		log:    log.New("Service", "Exchange"),
		server: nil,
        ctx:    ctx,
	}

    return service
}

func (p *exchangeService) Protocols() []p2p.Protocol {
	return []p2p.Protocol{{
        Name:     "partition",
        Version:  1,
        Length:   5,
        Run:      p.ShardChannel,
        NodeInfo: p.NodeInfo,
        PeerInfo: p.PeerInfo,
    },
  }
}

func (p *exchangeService) APIs() []rpc.API {
	return []rpc.API{
		{
			Namespace: "dex",
			Version:   "1.0",
			Service:   &RPCService{
                service:   p,
                dbOrders:  newExchangeDatabase(p.ctx),
            },
			Public:    true,
		},
        {
			Namespace: "kyc",
			Version:   "1.0",
            Service:   kyc.NewRpcService(p.ctx),
			Public:    true,
		},
	}
}

func (p *exchangeService) Start(server *p2p.Server) error {
	p.server = server
	p.log.Info("Exchange service starting")
	return nil
}

func (p *exchangeService) Stop() error {
	p.log.Info("Exchange service stopping")
	return nil
}

func (p *exchangeService) NodeInfo() interface{} {
	/*return struct {
		Received int64 `json:"received"`
	}{
		atomic.LoadInt64(&p.received),
	}*/
    log.Error("node", "info", p.server)
    return "test"
}

func (p *exchangeService) PeerInfo(nodeId discover.NodeID) interface{} {
    //..???
    return p.server.Self().ID.String()

	/*return struct {
		Received int64 `json:"received"`
	}{
		atomic.LoadInt64(&p.received),
	}*/
    //log.Error("peer", "info", p.server, "node", nodeId)
    //return "test"
}

func (p *exchangeService) ShardChannel(netPeer *p2p.Peer, rw p2p.MsgReadWriter) error {
    peer, err := newPeer(netPeer, rw)
    if err != nil {
        return err
    }

    return peer.Start()
}