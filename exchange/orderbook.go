package exchange

import (
    "github.com/paulbellamy/ratecounter"
    "github.com/Skyhark-Projects/dex/p2p/discover"
    "github.com/Skyhark-Projects/dex/common"
    "github.com/Skyhark-Projects/dex/log"
    "strings"
    "encoding/json"
    "bytes"
    "errors"
    "math"
    "sync"
    "time"
)

var rateCounterSec = ratecounter.NewRateCounter(time.Second)
var rateCounterMin = ratecounter.NewRateCounter(60 * time.Second)

type rawFlattenOrderList []*Order
type flattenOrderList []orderBookItem

type lint struct {
    Current *Order
    Prev *lint
    Next *lint
    Rate float64
}

type lintOfferList struct {
    offerLints map[string]*lint
    locks map[string]*sync.RWMutex
    mutex *sync.RWMutex
}

func (l *lintOfferList) Lock() {
    l.mutex.Lock()
}

func (l *lintOfferList) Unlock() {
    l.mutex.Unlock()
}

func (l *lintOfferList) RLock() {
    l.mutex.RLock()
}

func (l *lintOfferList) RUnlock() {
    l.mutex.RUnlock()
}

func (l *lintOfferList) getOfferLock(iso string) *sync.RWMutex {
    l.Lock();
    defer l.Unlock()
    
    lock, exists := l.locks[iso]
    if exists == false {
        lock = &sync.RWMutex{}
        l.locks[iso] = lock
    }

    return lock
}

func (l *lintOfferList) LockOffer(iso string) {
    l.getOfferLock(iso).Lock()
}

func (l *lintOfferList) UnlockOffer(iso string) {
    l.getOfferLock(iso).Unlock()
}

func (l *lintOfferList) Get(name string) (*lint, bool) {
    l.RLock();
    val, exists := l.offerLints[name];
    l.RUnlock()
    return val, exists
}

func (l *lintOfferList) GetVal(name string) *lint {
    val, _ := l.Get(name)
    return val
}

func (l *lintOfferList) Put(name string, lint *lint) {
    l.Lock();
    l.offerLints[name] = lint
    l.Unlock()
}

func (l *lintOfferList) Clear() {
    l.Lock();
    l.offerLints = map[string]*lint{}
    l.Unlock()
}

//----------------

type lintRequestList map[string]lintOfferList

func (l *lintRequestList) Lock() {
    lintsLock.Lock()
}

func (l *lintRequestList) Unlock() {
    lintsLock.Unlock()
}

func (l *lintRequestList) RLock() {
    lintsLock.RLock()
}

func (l *lintRequestList) RUnlock() {
    lintsLock.RUnlock()
}

func (l lintRequestList) Get(name string) (lintOfferList, bool) {
    l.RLock();
    val, exists := l[name];
    l.RUnlock()
    return val, exists
}

func (l lintRequestList) GetVal(name string) lintOfferList {
    val, _ := l.Get(name)
    return val
}

func (l lintRequestList) Put(name string, lint lintOfferList) {
    l.Lock();
    l[name] = lint
    l.Unlock()
}


var (
    lints = lintRequestList{}
    lintsLock = &sync.RWMutex{}
)

type OrderBook struct {
    BaseIso string
    MarketIso string
    Bid *lint
    Ask *lint
}

type orderBookItem struct {
    Quantity float64
    Rate float64
    TimeAgo *int `json:"timeAgo,omitempty"`
    Identity string
}

type FlatOrderBook struct {
    BaseIso string
    MarketIso string
    BidDepth uint
    AskDepth uint
    Bid flattenOrderList
    Ask flattenOrderList
}

type RawFlatOrderBook struct {
    BaseIso string
    MarketIso string
    Bid rawFlattenOrderList
    Ask rawFlattenOrderList
}

func Round(x, unit float64) float64 {
	return math.Round(x/unit) * unit
}

//--------------------

func GetRequestLints(request string) lintOfferList {
    request = strings.ToUpper(request)

    lints.RLock()
    mainList, ok := lints[request]
    lints.RUnlock()

    if(ok == false) {
        mainList = lintOfferList{
            offerLints: map[string]*lint{},
            mutex: &sync.RWMutex{},
            locks: map[string]*sync.RWMutex{},
        }

        lints.Lock()
        lints[request] = mainList
        lints.Unlock()
    }

    return mainList
}

func AppendOrderLocaly(order *Order) error {
    if(order == nil || order.RequestedToken.Iso == "" || order.RequestedToken.Quantity <= 0 || order.OfferedToken.Iso == "" || order.OfferedToken.Quantity <= 0) {
        return errors.New("Empty Order provided");
    }

    if(order.RequestedToken.Address == "") {
        return errors.New("Empty request address provided");
		}
		
    rate := order.Rate()
    orderItem := lint{
        Current: order,
        Prev: nil,
        Next: nil,
        Rate: rate,
    }

    //------------

    main := GetRequestLints(order.RequestedToken.Iso)
    lintItem, ok := main.Get(order.OfferedToken.Iso)

    //Create first lint item
    if(ok == false) {
        main.Put(order.OfferedToken.Iso, &orderItem)
        return nil
    }

    main.LockOffer(order.OfferedToken.Iso)
    defer main.UnlockOffer(order.OfferedToken.Iso)

    //Insert item at right place (sorted by rate)
    id := order.Id()

    var lastItem *lint
    itemFound := false

    for(lintItem != nil && rate <= lintItem.Rate) {
        if(lintItem.Current.Id() == id) {
            itemFound = true
            if lintItem.Current.StateId >= order.StateId {
                if lintItem.Current.StateId > order.StateId { 
                    return errors.New("A newer state is already available for this order");
                }

                return errors.New("Order already exists");
            }

            //remove order
            removeOrderAtPosition(main, lintItem)
        } else {
            lastItem = lintItem
        }

        lintItem = lintItem.Next
    }

    orderItem.Next = lintItem
    orderItem.Prev = lastItem

    if(lintItem != nil) {
        lintItem.Prev = &orderItem
    }

    if(lastItem != nil) {
        lastItem.Next = &orderItem
    } else {
        main.Put(order.OfferedToken.Iso, &orderItem)
		}

    if itemFound == false && orderItem.Next != nil {
        for(lintItem != nil) {
            if lintItem.Current.Id() == id {
                removeOrderAtPosition(main, lintItem)
                break;
            }

            lintItem = lintItem.Next
        }
    }

    return nil
}

func BroadcastOrder(order *Order, ignoredPeers ...discover.NodeID) {
    PeersPool.SendOrder(order, ignoredPeers...)

    rateCounterSec.Incr(1)
    rateCounterMin.Incr(1)
}

func AppendOrder(order *Order, ignoredPeers ...discover.NodeID) error {
    err := AppendOrderLocaly(order);
    if err != nil {
        return err;
    }

    //BroadcastOrder(order, ignoredPeers...)
    return err
}

func GetOrder(id string, reqIso string, offIso string) *Order {
    lints.RLock()
    main, ok := lints[strings.ToUpper(reqIso)]
    lints.RUnlock()
    if(ok == false) {
        return nil
    }

    lintItem, ok := main.Get(strings.ToUpper(offIso))
    if(ok == false) {
        return nil
    }

    for(lintItem != nil) {
        if(lintItem.Current.Id() == id) {
            return lintItem.Current
        }

        lintItem = lintItem.Next
    }

    return nil
}

//--------------------

func (l *lint) Flatten(limit uint, timeago bool, reverseRate ...bool) flattenOrderList {
    list := flattenOrderList{}
    item := l
    last := l
    var i uint = 0;

    rev := false 
    if(len(reverseRate) > 0) {
        rev = reverseRate[0]
    }

    enableLiquidityChecks := true
    now := 0
    if(timeago) {
        now = int(time.Now().UnixNano() / int64(time.Millisecond))
    }

    for {
        spendSumOfIds := map[common.Hash]map[string]uint{}

        for(item != nil) {
            if(i >= limit) {
                break;
            }

            if enableLiquidityChecks {
                if _, oke := spendSumOfIds[item.Current.Identity.IdentityHash]; !oke {
                    spendSumOfIds[item.Current.Identity.IdentityHash] = map[string]uint{}
                }

                available, err := item.Current.Protocol.VerifyFunds(item.Current, spendSumOfIds)
                if err != nil {
                    log.Error("error while checking if funds are available", "error", err)
                }

                if !available {
                    item = item.Next
                    continue
                }
            }

            var quantity, rate float64
            if (rev) {
                rate = Round(item.Current.RevRate(), 0.00000001);
                quantity = float64(item.Current.OfferedToken.Quantity)
            } else {
                rate = Round(item.Current.Rate(), 0.00000001);
                quantity = float64(item.Current.RequestedToken.Quantity)
            }

            listItem := orderBookItem{
                Quantity: quantity / 100000000.0,
                Rate: rate,
                Identity: item.Current.Identity.GetToken(),
            }

            if(timeago) {
                val := now - int(item.Current.Timestamp)
                listItem.TimeAgo = &val
            }

            list = append(list, listItem)
            item = item.Next
            last = item //Not restarting from current item, but from next to prevent double presence of item in list
            i++
        }

        if i >= limit || !enableLiquidityChecks {
            break
        }

        item = last
        enableLiquidityChecks = false
    }

    return list
}

func (l *lint) RawFlatten() rawFlattenOrderList {
    list := rawFlattenOrderList{}
    item := l

    for(item != nil) {
        list = append(list, item.Current)
        item = item.Next
    }

    return list
}

func (l *lint) GetDepth() uint {
    var depth uint = 0;
    
    item := l
    for(item != nil) {
        depth++
        item = item.Next
    }

    return depth
}

func GetOrderBook(request string, offer string) *OrderBook {
    request = strings.ToUpper(request)
    offer = strings.ToUpper(offer)

    mainRequest := GetRequestLints(request)
    mainOffer := GetRequestLints(offer)

    return &OrderBook{
        BaseIso: offer,
        MarketIso: request,
        Bid: mainRequest.GetVal(offer),
        Ask: mainOffer.GetVal(request),
    }
}

func (book *OrderBook) Flatten(timeago bool, limits ...uint) *FlatOrderBook {
    var limit uint = 10;
    if(len(limits) > 0) {
        limit = limits[0]
    }

    return &FlatOrderBook{
        BaseIso: book.BaseIso,
        MarketIso: book.MarketIso,
        BidDepth: book.Bid.GetDepth(),
        AskDepth: book.Ask.GetDepth(),
        Bid: book.Bid.Flatten(limit, timeago, false),
        Ask: book.Ask.Flatten(limit, timeago, true),
    }
}

func (book *OrderBook) RawFlatten() *RawFlatOrderBook {
    return &RawFlatOrderBook{
        BaseIso: book.BaseIso,
        MarketIso: book.MarketIso,
        Bid: book.Bid.RawFlatten(),
        Ask: book.Ask.RawFlatten(),
    }
}

func (book *OrderBook) ToJSON() string {
    buf := new(bytes.Buffer)
    json.NewEncoder(buf).Encode(book.Flatten(false))
    return buf.String()
}

func (book *OrderBook) GetBid() uint {
    if book.Bid != nil {
        return uint(book.Bid.Current.RevRate() * 100000000)
    }

    return 0
}

func (book *OrderBook) GetAsk() uint {
    if book.Ask != nil {
        return uint(book.Ask.Current.Rate() * 100000000)
    }

    return 0
}