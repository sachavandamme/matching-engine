pragma solidity ^0.4.0;
pragma experimental ABIEncoderV2;

contract C {

    struct Id {
        string Token;
        string Country;
        string Authority;
        string AuthorityCountry;
        bool Trusted;
    }

    struct OrderToken {
        uint   Quantity;
        string Iso;
        string Address;
    }

    struct Order {
        Id Identity;
        int64 Timestamp;
        uint Ttl;
        bool StrictIdentity;
        OrderToken RequestedToken;
        OrderToken OfferedToken;
        //mapping(string=>string) MetaData;

        uint MatchedQuantity;
    }

    struct Match {
        Order Output;
        uint SendQuantity;
    }

    struct Response {
        bool Matched;
        bytes32[] Dependencies;
        bytes32[] Routes;
        uint[] RouteValues;
    }

    Response private result;

    //-------------------------------------------------

    function addRoute(bytes32 addr, uint value) public {
        result.Routes.push(addr);
        result.RouteValues.push(value);
    }

    function addDependency(bytes32 orderId) public {
        result.Dependencies.push(orderId);
    }

    //-------------------------------------------------

    function execute(Order myOrder, Order matchedOrder, Match[] MatchedList) public returns(bool, bytes32[]) {
        result.Matched = verifyMatch(myOrder, matchedOrder, MatchedList);
        return (result.Matched, result.Dependencies);
    }

    function verifyMatch(Order myOrder, Order matchedOrder, Match[] MatchedList) private returns(bool) {
        return true;
    }
}

contract OrderContract is C {
 
    function verifyMatch(Order myOrder, Order matchedOrder, Match[] MatchedList) private returns(bool) {
        return true;
    }

}
