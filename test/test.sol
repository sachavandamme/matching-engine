pragma solidity ^0.4.0;
pragma experimental ABIEncoderV2;

contract OrderContract {

    function verifyMatch() private view returns (bool) {
        //Custom code here..

        return inputOrder.StrictIdentity;
    }

    //-------------------------------------------------------------

    struct Id {
        string Token;
        string Country;
        string Authority;
        string AuthorityName;
    }

    struct OrderToken {
        uint   Quantity;
        string Iso;
        string Address;
    }

    struct Order {
        Id Identity;
        int64 Timestamp;
        uint Ttl;
        bool StrictIdentity;
        OrderToken RequestedToken;
        OrderToken OfferedToken;
        //mapping(string=>string) MetaData;
        
        uint MatchedQuantity;
    }

    Order inputOrder;
    Order outputOrder;
    Order[] matches; //list of matched output orders containing matched quantity per order
    string[] routes;
    uint[] routeValues;
    string[] dependencies;
    Id lastIdentity;
    
    function handleMatch() public view returns (bool, string[], string[], uint[]) {
        bool Match = verifyMatch();
        return (Match, dependencies, routes, routeValues);
    }

    function addDependency(string OrderId) private {
        dependencies.push(OrderId);
    }

    function addRoute(string Address, uint Amount) private {
        routes.push(Address);
        routeValues.push(Amount);
    }
    
    function pushIdentity(string Token, string Country, string Authority, string AuthorityName) public {
        lastIdentity.Token = Token;
        lastIdentity.Country = Country;
        lastIdentity.Authority = Authority;
        lastIdentity.AuthorityName = AuthorityName;
    }
    
    function setInputOrder(string IdToken, int64 Timestamp, uint Ttl, bool StrictIdentity, uint MatchedQuantity,
        uint RQuantity, string RIso, string RAddress, 
        uint OQuantity, string OIso, string OAddress) public {

        inputOrder.Identity = Id(IdToken, lastIdentity.Country, lastIdentity.Authority, lastIdentity.AuthorityName);
        inputOrder.Timestamp = Timestamp;
        inputOrder.Ttl = Ttl;
        inputOrder.StrictIdentity = StrictIdentity;
        inputOrder.MatchedQuantity = MatchedQuantity;
        inputOrder.RequestedToken = OrderToken(RQuantity, RIso, RAddress);
        inputOrder.OfferedToken = OrderToken(OQuantity, OIso, OAddress);

        //mapping(string=>string) MetaData;
    }
    
    
    function setOutputOrder(string IdToken, int64 Timestamp, uint Ttl, bool StrictIdentity, uint MatchedQuantity,
        uint RQuantity, string RIso, string RAddress, 
        uint OQuantity, string OIso, string OAddress) public {
        
        outputOrder.Identity = Id(IdToken, lastIdentity.Country, lastIdentity.Authority, lastIdentity.AuthorityName);
        outputOrder.Timestamp = Timestamp;
        outputOrder.Ttl = Ttl;
        outputOrder.StrictIdentity = StrictIdentity;
        outputOrder.MatchedQuantity = MatchedQuantity;
        outputOrder.RequestedToken = OrderToken(RQuantity, RIso, RAddress);
        outputOrder.OfferedToken = OrderToken(OQuantity, OIso, OAddress);

        //mapping(string=>string) MetaData;
    }
    
    
    function pushMatch(string IdToken, int64 Timestamp, uint Ttl, bool StrictIdentity, uint MatchedQuantity,
        uint RQuantity, string RIso, string RAddress, 
        uint OQuantity, string OIso, string OAddress) public {

        Order memory Match;
        
        Match.Identity = Id(IdToken, lastIdentity.Country, lastIdentity.Authority, lastIdentity.AuthorityName);
        Match.Timestamp = Timestamp;
        Match.Ttl = Ttl;
        Match.StrictIdentity = StrictIdentity;
        Match.MatchedQuantity = MatchedQuantity;
        Match.RequestedToken = OrderToken(RQuantity, RIso, RAddress);
        Match.OfferedToken = OrderToken(OQuantity, OIso, OAddress);

        //mapping(string=>string) MetaData;
        
        matches.push(Match);
    }

}
