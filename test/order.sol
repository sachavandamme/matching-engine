pragma solidity ^0.4.0;
pragma experimental ABIEncoderV2;

contract C {

    struct Id {
        string Token;
        string Country;
        string Authority;
        string AuthorityName;
        bool Trusted;
    }

    struct OrderToken {
        uint   Quantity;
        string Iso;
        string Address;
    }

    struct Order {
        Id Identity;
        int64 Timestamp;
        uint Ttl;
        bool StrictIdentity;
        OrderToken RequestedToken;
        OrderToken OfferedToken;
        //mapping(string=>string) MetaData;
        
        uint MatchedQuantity;
    }

    struct Response {
        bool Matched;
        string[] Dependencies;
        string[] Routes;
        //uint[] RouteValues;
    }

}

contract OrderContract is C {

    //Response result;

    function test() public returns(Response) {
        string[] storage dependencies;
        dependencies.push("test");

        Response memory result = Response(true, dependencies, dependencies);
        //result.Matched = true;
        //result.Dependencies.push("abc");

        return result;
    }

    function handleMatch(Order ownOrder, Order matchOrder, Order[] groupedMatches) public pure returns(Response result) {
        
    }

}
