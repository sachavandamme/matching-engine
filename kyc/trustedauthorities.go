package kyc

import (
    "github.com/Skyhark-Projects/dex/log"
    "io/ioutil"
    "crypto/x509"
    "encoding/pem"
)

var (
  root = NewRoot()
)

type TrustedAuthorities struct {
    roots *x509.CertPool
}

func NewRoot() *TrustedAuthorities {
    root := TrustedAuthorities{}
    root.roots = x509.NewCertPool()

    return &root
}

func GetRoot() *TrustedAuthorities {
    return root
}

func (t TrustedAuthorities) AddAuthority(cert string) bool {
    ok := t.roots.AppendCertsFromPEM([]byte(cert))

    if !ok {
        return false
    }

    return true
}

func (t TrustedAuthorities) AddAuthorityFile(path string) bool {
    dat, err := ioutil.ReadFile(path)

    if(err != nil) {
        log.Error("TrustedAuthorities", "AddAuthorityFile", err)
        return false
    }

    return t.AddAuthority(string(dat))
}

func (t TrustedAuthorities) VerifyCertificate(cert *x509.Certificate) ([][]*x509.Certificate, bool) {
    opts := x509.VerifyOptions{ Roots: t.roots }

    chain, err := cert.Verify(opts);

    if err != nil {
        log.Error("TrustedAuthorities", "VerifyCertificate", err.Error())
        return chain, false
    }

    return chain, true
}

func (t TrustedAuthorities) VerifyPEM(certPEM string) ([][]*x509.Certificate, bool) {
    block, err := pem.Decode([]byte(certPEM))

    if block == nil {
        log.Error("TrustedAuthorities", "VerifyPEM", err)
        return [][]*x509.Certificate{}, false;
    }

    cert, err2 := x509.ParseCertificate(block.Bytes)
    if err2 != nil {
        log.Error("TrustedAuthorities", "VerifyPEM", err2.Error())
        return [][]*x509.Certificate{}, false;
    }

    return t.VerifyCertificate(cert)
}

func (t TrustedAuthorities) VerifyPEMFromFile(path string) ([][]*x509.Certificate, bool) {
    dat, err := ioutil.ReadFile(path)

    if(err != nil) {
        log.Error("TrustedAuthorities", "VerifyPEMFromFile", err)
        return [][]*x509.Certificate{}, false;
    }

    return t.VerifyPEM(string(dat))
}

func (t TrustedAuthorities) VerifyIdentity(identity *Identity) ([][]*x509.Certificate, bool) {
    return t.VerifyCertificate(identity.Certificate)
}