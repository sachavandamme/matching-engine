package kyc

import (
    "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/common"
    "github.com/Skyhark-Projects/dex/rlp"
    dexCrypto "github.com/Skyhark-Projects/dex/crypto"
    "io/ioutil"
    "crypto"
    "crypto/x509"
    "crypto/sha256"
    "crypto/rsa"
    "crypto/rand"
    "encoding/pem"
    "bytes"
    "errors"
    "io"
    "sync/atomic"
)

type Identity struct {
    id atomic.Value `json:"id"       rlp:"nil"`
    Certificate     *x509.Certificate
    PrivateKey      *rsa.PrivateKey

    FirstName       string
    LastName        string
    PassportNumber  string
    State           string
    City            string
    Zip             string
    Address         string
    Address2        string
}

func New(certPEM string) *Identity {
    id := Identity{}

    if(id.ParsePem(certPEM) == false) {
        return nil
    }

    return &id
}

func FromFile(path string) *Identity {
    id := Identity{}
    
    if(id.LoadPem(path) == false) {
        return nil
    }
    
    return &id
}

func (id *Identity) ParsePrivateKey(certPEM string) error {
    block, _ := pem.Decode([]byte(certPEM))

    if block == nil {
        return errors.New("Could not decode private key");
    }
    
    if(block.Type != "RSA PRIVATE KEY") {
        return errors.New("Wrong PEM type")
    }

    cert, err := x509.ParsePKCS1PrivateKey(block.Bytes)
    if err != nil {
        return err
    }

    id.PrivateKey = cert
    return nil
}

func (id *Identity) ParsePemBytes(certPEM []byte) bool {
    block, err := pem.Decode(certPEM)

    if block == nil {
        log.Error("Identity", "New", err)
        return false
    }

    if(block.Type == "CERTIFICATE") {
        cert, err2 := x509.ParseCertificate(block.Bytes)
        if err2 != nil {
            log.Error("Identity", "ParsePem", err2.Error())
            return false
        }

        id.Certificate = cert
        identitiesDb.Put(id)
    } else if(block.Type == "RSA PRIVATE KEY") {
        cert, err := x509.ParsePKCS1PrivateKey(block.Bytes)

        if err != nil {
            log.Error("Identity", "ParsePem", err.Error())
            return false
        }

        id.PrivateKey = cert
    } else {
        log.Error("Identity", "Unkown certificate type", block.Type)
        return false
    }

    return true
}

func (id *Identity) ParsePem(certPEM string) bool {
    return id.ParsePemBytes([]byte(certPEM))
}

func (id *Identity) LoadPem(path string) bool {
    data, err := ioutil.ReadFile(path)

    if(err != nil) {
        log.Error("Identity", "LoadPem", err)
        return false
    }

    return id.ParsePemBytes(data)
}

func (id *Identity) GetToken() string {
    if id.Certificate == nil {
        return ""
    }

    return id.Certificate.Subject.CommonName
}

func (id *Identity) Id() common.Hash {
    if hash := id.id.Load(); hash != nil {
        return hash.(common.Hash)
    }

    hash := common.BytesToHash(dexCrypto.Keccak256( id.Certificate.Raw ))
    id.id.Store(hash)
    return hash
}

func (id *Identity) GetCountry() string {
    if len(id.Certificate.Subject.Country) == 0 {
        return ""
    }

    return id.Certificate.Subject.Country[0]
}

func (id *Identity) GenerateDataHash() string {
    var buffer bytes.Buffer
    buffer.WriteString(id.FirstName)
    buffer.WriteString(id.LastName)
    buffer.WriteString(id.PassportNumber)
    buffer.WriteString(id.GetCountry())
    buffer.WriteString(id.State)
    buffer.WriteString(id.City)
    buffer.WriteString(id.Zip)
    buffer.WriteString(id.Address)
    buffer.WriteString(id.Address2)
    
    h := sha256.New()
    h.Write([]byte(buffer.String()))

    return common.Bytes2Hex(h.Sum(nil))
}

func (id *Identity) VerifiyInformations() bool {
    return id.GetToken() == id.GenerateDataHash()
}

func (id *Identity) IsValid(ca ...*TrustedAuthorities) bool {
    
    if !id.VerifiyInformations() {
        return false
    }

    if len(ca) > 0 {
        return id.IsTrusted(ca[0])
    }

    return id.IsTrusted()
}

func (id *Identity) IsTrusted(ca ...*TrustedAuthorities) bool {
    _, trusted := id.GetTrustedChain(ca...);
    return trusted;
}

func (id *Identity) GetTrustedChain(ca ...*TrustedAuthorities) ([][]*x509.Certificate, bool) {
    if len(ca) > 0 {
        return ca[0].VerifyIdentity(id);
    }
    
    return GetRoot().VerifyIdentity(id);
}

func (id *Identity) Sign(message []byte) ([]byte, error) {
  h := sha256.New()
	h.Write(message)
	d := h.Sum(nil)

	return rsa.SignPKCS1v15(rand.Reader, id.PrivateKey, crypto.SHA256, d)
}

func (id *Identity) SignString(message string) ([]byte, error) {
    return id.Sign([]byte(message))
}

func (id *Identity) VerifySignature(signature []byte, message []byte) error {
    h := sha256.New()
    h.Write(message)
    d := h.Sum(nil)

    key := id.Certificate.PublicKey.(*rsa.PublicKey)
    return rsa.VerifyPKCS1v15(key, crypto.SHA256, d, signature)
}

func (id *Identity) EncodeRLP(w io.Writer) error {
    return rlp.Encode(w, &id.Certificate.Raw)
}

func (id *Identity) DecodeRLP(s *rlp.Stream) error {
    raw := []byte{}

    err := s.Decode(&raw)
    if err != nil {
        return err
    }

    cert, err := x509.ParseCertificate(raw)
    if err != nil {
        return err
    }

    id.Certificate = cert
    return nil
}