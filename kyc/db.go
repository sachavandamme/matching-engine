package kyc

import (
    "github.com/Skyhark-Projects/dex/ethdb"
    "github.com/Skyhark-Projects/dex/node"
    "github.com/Skyhark-Projects/dex/log"
    "github.com/Skyhark-Projects/dex/common"
    "github.com/Skyhark-Projects/dex/p2p"
    "github.com/Skyhark-Projects/dex/rlp"
    "github.com/Skyhark-Projects/dex/event"
    "github.com/syndtr/goleveldb/leveldb/iterator"
    "errors"
    "sync"
    "time"
)


//ToDo remove unused identities from memory
type IdentityDb struct {
    memory  map[common.Hash]*Identity
    lock    *sync.RWMutex
    db      *ethdb.LDBDatabase
    feed    *event.Feed
}

func (l *IdentityDb) Lock() {
    l.lock.Lock()
}

func (l *IdentityDb) Unlock() {
    l.lock.Unlock()
}

func (l *IdentityDb) RLock() {
    l.lock.RLock()
}

func (l *IdentityDb) RUnlock() {
    l.lock.RUnlock()
}

func (l *IdentityDb) NewIterator() (iterator.Iterator) {
    return l.db.NewIterator()
}

func (id *IdentityDb) Get(hash common.Hash) (*Identity, error) {
    id.RLock()
    defer id.RUnlock()
    
    if id, oke := id.memory[hash]; oke {
        return id, nil
    }

    data, err := id.db.Get(hash.Bytes())
    if err != nil {
        //ToDo if error is not "leveldb: not found" return error, else return nil
        return nil, nil
    }

    identity := &Identity{}
    if err = rlp.DecodeBytes(data, identity); err != nil {
        return nil, err
    }

    //id.memory[hash] = identity todo set in memory & release when not used since x seconds/minutes
    return identity, nil
}

func (id *IdentityDb) GetByToken(token string) *Identity {
    id.RLock()
    defer id.RUnlock()

    for _, id := range id.memory {
        if id.GetToken() == token {
            return id
        }
    }

    //Check from leveldb
    it := id.NewIterator()
    defer it.Release()

	for it.Next() {
        identity := &Identity{}
        if err := rlp.DecodeBytes(it.Value(), identity); err != nil {
            continue
        }

        if identity.GetToken() == token {
            return identity
        }
    }

    return nil
}

func (id *IdentityDb) Put(identity *Identity) error {
    if identity.Certificate == nil {
        return errors.New("Identity certificate not found")
    }

    token    := identity.Id()
    old, err := id.Get(token)
    if err != nil {
        return err
    }
    
    
    id.Lock()
    id.memory[token] = identity
    data, err := rlp.EncodeToBytes(identity)
	if err != nil {
        id.Unlock()
		return err
	}

    id.db.Put(token.Bytes(), data)
    id.Unlock()

    if(old == nil) {
        id.feed.Send(id)
    } else if(old.PrivateKey != nil && identity.PrivateKey == nil) {
        identity.PrivateKey = old.PrivateKey
    }

    return nil
}

//--------------------------------------------------

var (
    identitiesDb *IdentityDb
)

func WaitInitialized() {
    for identitiesDb == nil {
        time.Sleep(20 * time.Millisecond)
    }
}

func Init(ctx *node.Node) {
    if(identitiesDb != nil) {
        log.Warn("Identities already initialized, trying to initialize twice.");
        return
    }

    lvldb, err := ethdb.NewLDBDatabase(ctx.ResolvePath("kyc"), 0, 0);
    if err != nil {
        log.Error("Could not initial kyc levedb", "error", err)
    }

    identitiesDb = &IdentityDb{
        db:     lvldb,
        feed:   new(event.Feed),
        lock:   &sync.RWMutex{},
        memory: map[common.Hash]*Identity{},
    }
}

func GetIdentity(hash common.Hash) *Identity {
    id, err := identitiesDb.Get(hash)
    if err != nil {
        log.Error("Could Not Get Identity", "error", err)
    }

    return id
}

func GetOwnIdentityByToken(token string) *Identity {
    return identitiesDb.GetByToken(token)
}

func BroadcastIdentities(rw p2p.MsgReadWriter, code uint64) error {
    it := identitiesDb.NewIterator()
    defer it.Release()

	for it.Next() {
        identity := &Identity{}
        if err := rlp.DecodeBytes(it.Value(), identity); err != nil {
            return err
        }

        if err := p2p.Send(rw, code, identity); err != nil {
            return err
        }
    }

    return nil
}

func BindPeerIdentities(rw p2p.MsgReadWriter, msgCode uint64, errC chan error) {
    ch := make(chan *Identity)
    sub := identitiesDb.feed.Subscribe(ch)
    defer sub.Unsubscribe()

    for {
        id := <-ch
        if err := p2p.Send(rw, msgCode, id); err != nil {
            errC <- err
            return
        }
    }
}