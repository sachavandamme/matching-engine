package kyc

import (
    "github.com/Skyhark-Projects/dex/accounts/abi"
    "github.com/Skyhark-Projects/dex/common"
)

type IdentitySignature struct {
    IdentityHash common.Hash
    Signature []byte
}

var idSignatureArguments = abi.Arguments{
    abi.Argument{
        Name: "Token",
        Type: getType("string"),
        Indexed: false,
    },
    abi.Argument{
        Name: "Country",
        Type: getType("string"),
        Indexed: false,
    },
    abi.Argument{
        Name: "Authority",
        Type: getType("string"),
        Indexed: false,
    },
    abi.Argument{
        Name: "AuthorityName",
        Type: getType("string"),
        Indexed: false,
    },
    abi.Argument{
        Name: "Trusted",
        Type: getType("bool"),
        Indexed: false,
    },
}

func (i *IdentitySignature) GetIdentity() *Identity {
    if(i == nil) {
        return nil;
    }

    return GetIdentity(i.IdentityHash)
}

func (i *IdentitySignature) GetToken() string {
    id := i.GetIdentity();
    if id == nil {
        return "";
    }

    return id.GetToken()
}

func (i *IdentitySignature) IsValidForInput(Id string) bool {
    //Find identity from token
    identity := i.GetIdentity()
    if(identity == nil) {
        return false
    }

    //Verifiy if identity signature is correct
    err := identity.VerifySignature(i.Signature, []byte(Id))
    if(err != nil) {
        return false
    }

    return true
}

func (i *IdentitySignature) IsValidForBytes(input []byte) bool {
    //Find identity from token
    identity := i.GetIdentity()
    if(identity == nil) {
        return false
    }

    //Verifiy if identity signature is correct
    err := identity.VerifySignature(i.Signature, input)
    if(err != nil) {
        return false
    }

    return true
}

func (i *IdentitySignature) IsTrusted() bool {    
    //Find identity from token
    identity := i.GetIdentity()
    if(identity == nil) {
        return false
    }

    //Verify if identity is trusted by a CA
    return identity.IsTrusted();
}

func (i *IdentitySignature) PackABI(trusted ...bool) ([]byte, error) {
    trust := false;
    if(len(trusted) > 0) {
        trust = trusted[0];
    }

    if(i == nil) {
        return idSignatureArguments.Pack("", "", "", "", trust);
    }

    //--------------

    identity := i.GetIdentity()
    var Country, Authority, AuthorityCountry string;

    if(identity != nil) {
        if(len(trusted) == 0) {
            trust = identity.IsTrusted();
        }
        
        Country = identity.GetCountry();
        issuer := identity.Certificate.Issuer;
        Authority = issuer.CommonName;
        
        if len(issuer.Country) != 0 {
            AuthorityCountry = issuer.Country[0];
        }
    }

    bts, err := idSignatureArguments.Pack(i.IdentityHash, Country, Authority, AuthorityCountry, trust);
    
    if(err != nil) {
        return bts, err;
    }

    return bts, nil;
}

func getType(name string) abi.Type {
    t, _ := abi.NewType(name);
    return t;
}