package kyc

import (
    "github.com/Skyhark-Projects/dex/node"
    "github.com/Skyhark-Projects/dex/common"
    "github.com/Skyhark-Projects/dex/rlp"
    "github.com/Skyhark-Projects/dex/log"
    "crypto/x509/pkix"
    "crypto/x509"
    "crypto/rand"
    "crypto/rsa"
    "encoding/pem"
    "math/big"
    "errors"
    "bytes"
    "time"
    "crypto/tls"
)

type RPCService struct {
    ctx      *node.ServiceContext
}

func NewRpcService(ctx *node.ServiceContext) (*RPCService) {
    return &RPCService{
        ctx: ctx,
    }
}

func (s *RPCService) PushIdentity(cert string, key string) (string, error) {
    id := New(cert);

    if id == nil {
        return "", errors.New("Could not parse certificate");
    }

    if id.ParsePem(key) == false {
        return id.GetToken(), errors.New("Could not parse private key");
    }

    return id.GetToken(), nil
}

func (s *RPCService) Get(hash common.Hash) (*ServiceIdentity, error) {
    id, err := identitiesDb.Get(hash)
    if err != nil {
        return nil, err
    }

    bufPublic := new(bytes.Buffer)
    pem.Encode(bufPublic, &pem.Block{Type: "CERTIFICATE", Bytes: id.Certificate.Raw})

    return &ServiceIdentity{
        Id:         id.Id(),
        Token:      id.GetToken(),
        Certificate: bufPublic.String(),
    }, nil
}

func (s *RPCService) GetByToken(token string) *ServiceIdentity {
    id := identitiesDb.GetByToken(token)
    if id == nil {
        return nil
    }

    bufPublic := new(bytes.Buffer)
    pem.Encode(bufPublic, &pem.Block{Type: "CERTIFICATE", Bytes: id.Certificate.Raw})

    return &ServiceIdentity{
        Id:         id.Id(),
        Token:      id.GetToken(),
        Certificate: bufPublic.String(),
    }
}

func (s *RPCService) List() ([]idListItem, error) {
    res := []idListItem{}
    it := identitiesDb.NewIterator()
    defer it.Release()

	for it.Next() {
        identity := &Identity{}
        if err := rlp.DecodeBytes(it.Value(), identity); err != nil {
            log.Error("kyc_list error", "error", err)
            return res, err
        }

        res = append(res, idListItem{
            Hash:   common.BytesToHash(it.Key()),
            Token:  identity.GetToken(),
        })
    }

    return res, nil
}

func (s *RPCService) Generate(caName string, serialNumber int64, commonName string, country string, retainIdentity bool) (map[string]string, error) {
    // Load CA
    caPath := s.ctx.ResolvePath("ca/" + caName)
	catls, err := tls.LoadX509KeyPair(caPath + ".crt", caPath + ".key")
	if err != nil {
        return map[string]string{}, err
	}

	ca, err := x509.ParseCertificate(catls.Certificate[0])
	if err != nil {
		return map[string]string{}, err
	}

	// Prepare certificate
	cert := &x509.Certificate{
		SerialNumber: big.NewInt(serialNumber),
		Subject: pkix.Name{
            CommonName:    commonName,
            Country:       []string{country},
			/*Organization:  []string{"ORGANIZATION_NAME"},
			Province:      []string{"PROVINCE"},
			Locality:      []string{"CITY"},
			StreetAddress: []string{"ADDRESS"},
			PostalCode:    []string{"POSTAL_CODE"},*/
		},
		NotBefore:    time.Now(),
		NotAfter:     time.Now().AddDate(10, 0, 0),
		SubjectKeyId: []byte{1, 2, 3, 4, 6},
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageCodeSigning},
		KeyUsage:     x509.KeyUsageDigitalSignature,
	}
	priv, _ := rsa.GenerateKey(rand.Reader, 2048)
	pub := &priv.PublicKey

	// Sign the certificate
	cert_b, err := x509.CreateCertificate(rand.Reader, cert, ca, pub, catls.PrivateKey)
    res := map[string]string{
        "id": "",
    }

	// Public key
    bufPublic := new(bytes.Buffer)
	pem.Encode(bufPublic, &pem.Block{Type: "CERTIFICATE", Bytes: cert_b})
    res["public"] = bufPublic.String()

	// Private key
    bufPrivate := new(bytes.Buffer)
	pem.Encode(bufPrivate, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})
    res["private"] = bufPrivate.String()

    //Push identity
    id := &Identity{
        PrivateKey: priv,
    }

    cert, err = x509.ParseCertificate(cert_b)
    id.Certificate = cert
    if err != nil {
        return map[string]string{}, err
    }

    if retainIdentity {
        identitiesDb.Put(id)
    }

    res["id"] = id.Id().String()
    return res, nil
}

func (s *RPCService) GenerateCA(organisation string, country string, region string, city string, address string, zip string) (map[string]string, error) {
	ca := &x509.Certificate{
		SerialNumber: big.NewInt(1653),
		Subject: pkix.Name{
			Organization:  []string{organisation},
			Country:       []string{country},
			Province:      []string{region},
			Locality:      []string{city},
			StreetAddress: []string{address},
			PostalCode:    []string{zip},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageCodeSigning},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}

	priv, _ := rsa.GenerateKey(rand.Reader, 2048)
	pub := &priv.PublicKey
	ca_b, err := x509.CreateCertificate(rand.Reader, ca, ca, pub, priv)
	if err != nil {
        return map[string]string{}, err
	}

    res := map[string]string{}

	// Public key
    bufPublic := new(bytes.Buffer)
	pem.Encode(bufPublic, &pem.Block{Type: "CERTIFICATE", Bytes: ca_b})
    res["public"] = bufPublic.String()

	// Private key
    bufPrivate := new(bytes.Buffer)
	pem.Encode(bufPrivate, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})
    res["private"] = bufPrivate.String()

    return res, nil
}

type ServiceIdentity struct {
    Id              common.Hash
    Token           string
    Certificate     string
}

type idListItem struct {
    Hash    common.Hash
    Token   string
}