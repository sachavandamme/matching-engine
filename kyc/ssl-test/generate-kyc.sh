#!/bin/bash

mkdir -p identity
cd identity

openssl genrsa -out identity.key 2048
openssl rsa -in identity.key -noout -text
openssl rsa -in identity.key -pubout -out identity.pubkey
openssl rsa -in identity.pubkey -pubin -noout -text
openssl req -new -key identity.key -out identity.csr
openssl req -in identity.csr -noout -text

openssl x509 -req -in identity.csr -CA ../ca/ca.crt -CAkey ../ca/ca.key -CAcreateserial -out identity.crt

cat identity.crt ../ca/ca.crt > identity.bundle.crt