#!/usr/local/bin/node

const crypto   = require('crypto');
const identity = require('./identity.json');

const serial = [
    'first_name',
    'last_name',
    'passport_number',
    'country',
    'state',
    'city',
    'zip',
    'address',
    'address2'
];

var output = '';

for(var name of serial) {
    output += identity[name] || '';
}

console.log(crypto.createHash('sha256').update(output, 'utf8').digest().toString('hex'))